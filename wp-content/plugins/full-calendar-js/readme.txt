=== Full Calendar Js ===
Contributors: Cerzky
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VZTNWWQ7XKY7E
Tags: calendar, google, xml, fullcalendar, full, simple, wp, event, events, calendars, multiple, jquery, javascript, js, skydrive
Requires at least: 2.8.6
Tested up to: 3.9
Stable tag: 1.6
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display multiple Calendar XML feeds into a jquery calendar. Works with Google Calendar and others.

== Description ==
FullCalendarJs allows you to save multiple XML feeds into a calendar id and display those calendar's as if they were one.
The plugin works specifically off of a jquery plugin. The Jquery plugin is written by Adam Shaw. This plugin is lightweight
 and is designed for ease of use and simplification. Why bloat your own database with calendar events when you can fetch 
 them all from Google Calendar.

More features to come.

**Settings Page:**
* Settings > Full Calendar Settings 

**Shortcode:**
* [fcal id ="1"]  //shows calendar of id 1 in the page or post

== Installation ==
1. Upload to the 'wp-contents/plugins' directory,
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==
= How do I get my Google Calendar XML feed? =
1. Login to google.
2. Click 'Calendar'.
3. Click the down arrow next to your calendar.
4. Click 'Share this calendar'.
5. Check 'Make this calendar public' if desired. Allows users to subscribe.
6. Click 'Calendar Details'.
7. Click 'XML' under Calendar Address(public) or Private Address.

== Screenshots ==

1. This is the front end view of the calendar on the page.
2. This is the settings page and the XML feed(s) used to display the calendar.

== Changelog ==
1.5 Version now invokes the today method so that the calendar displays the current month on load without clicking 'today'.
1.6 Removed settings which were prototypes. Added helper notifications in the shortcode area.
== Upgrade Notice ==