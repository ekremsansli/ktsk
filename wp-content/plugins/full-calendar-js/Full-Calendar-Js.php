<?php
/*
	Plugin Name: Full Calendar Js
	Plugin URI: 
	Description: Allows you to display calendars using Google XML or other XML/RSS feed.
	Version: 1.6
	Author: Brandon Hogue
	Author URI: 
	License: GPL2
*/
/*  Copyright 2013  Brandon Hogue  (email : PLUGIN AUTHOR EMAIL)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

	//register scripts required by Fullcalender js
function fc_enqueue_scripts(){	

   wp_enqueue_script('jquery');
  // wp_enqueue_script('jquery-ui-core');
  // wp_enqueue_script('jquery-ui-widget');
  // wp_enqueue_script('jquery-ui-position');


	wp_register_style('fc_css', plugins_url('js/fullcalendar-1.5.4/fullcalendar/fullcalendar.css', __FILE__));
	
	//wp_register_script('fc_fc', plugins_url('js/fullcalendar-1.5.4/fullcalendar/fullcalendar.js', __FILE__));
	wp_register_script('fc_gcal', plugins_url('js/fullcalendar-1.5.4/fullcalendar/gcal.js', __FILE__));
	//wp_register_script('fc_agendamod', plugins_url('js/fullcalendar-1.5.4/fullcalendar/fullcalendaragendamod.js',__FILE__));
	//wp_register_script('fc_original', plugins_url('js/fullcalendar-1.5.4/fullcalendar/fullcalendaroriginal.js', __FILE__));
	//wp_register_script('fc_moment', plugins_url('js/fullcalendar-1.5.4/fullcalendar/moment.js', __FILE__));
    wp_register_script('fc_original', plugins_url('js/fullcalendar-1.5.4/fullcalendar/fullcalendaroriginal.js', __FILE__), array('jquery', 'jquery-ui-core','jquery-ui-widget','jquery-ui-position'));
    
	   
   


   
	wp_enqueue_script('fc_original');	
	wp_enqueue_script('fc_gcal');

	
	wp_enqueue_style('fc_css');

}
if (!is_admin()) add_action("wp_enqueue_scripts", "fc_enqueue_scripts", 11);

//Loads Fullcalender in Head  use  <div id='calendar'></div> to display
function render_calendar(){
//Calendar [1]
?>  
<script type='text/javascript'>
<?php $calendar = "#"; ?>
<?php $calendar .= get_option('fc_option_calendar'); ?>
jQuery(document).ready(function($) {
    // $() will work as an alias for jQuery() inside of this function


$(document).ready(function() {	
    $('<?php echo $calendar ?>').fullCalendar({
    	header: {
    		left: 'prev, next today, agenda',
    		center: 'title',
    		right: 'month, agendaWeek, agendaDay',
    		ignoreTimezone: false    		
    	},
    	 // Multiple Sources
        eventSources: [
        '<?php echo get_option('fc_option_feed1'); ?>',
        '<?php echo get_option('fc_option_feed2'); ?>',
        '<?php echo get_option('fc_option_feed3'); ?>'
        ],        
        events: {
        	//http://www.google.com/calendar/feeds/centre.edu_5f119bnujdfol3gs59h0driv9c%40group.calendar.google.com/public/basic
            //url: '<?php echo get_option('fc_option_feed1'); ?>',
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago', // an option!        	
        },
        	month: true,
        	week: true,
        	day: true,
        	agenda: true,
        	basic: true,
        	//showAgendaButton: true        	
    });
    $('<?php echo $calendar ?>').fullCalendar('today');
});
});
</script>
<script type='text/javascript'>
//Calendar [2]
<?php $calendar = "#"; ?>
<?php $calendar .= get_option('fc2_option_calendar'); ?>
jQuery(document).ready(function($) {
    // $() will work as an alias for jQuery() inside of this function

$(document).ready(function() {	
    $('<?php echo $calendar ?>').fullCalendar({
    	header: {
    		left: 'prev, next today, agenda',
    		center: 'title',
    		right: 'month, agendaWeek, agendaDay',
    		ignoreTimezone: false    		
    	},
    	 // Multiple Sources
        eventSources: [
        '<?php echo get_option('fc2_option_feed1'); ?>',
        '<?php echo get_option('fc2_option_feed2'); ?>',
        '<?php echo get_option('fc2_option_feed3'); ?>'
        ],        
        events: {
        	//http://www.google.com/calendar/feeds/centre.edu_5f119bnujdfol3gs59h0driv9c%40group.calendar.google.com/public/basic
            //url: '<?php echo get_option('fc_option_feed1'); ?>',
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago', // an option!        	
        },
        	month: true,
        	week: true,
        	day: true,
        	agenda: true,
        	basic: true,
        	//showAgendaButton: true        	
    });
    $('<?php echo $calendar ?>').fullCalendar('today');
});
});
</script>

<?php
}//end php function
add_action('wp_head', 'render_calendar');

// Dashboard or Settings Menu

function fc_create_menu(){
	//create new top-level menu
	add_options_page('Full Calender Settings Page', 'Full Calender Settings', 'add_users', __FILE__,
	'fc_settings_page');

	//call register settings function
	add_action('admin_init', 'fc_register_settings');
}
add_action('admin_menu', 'fc_create_menu');

function fc_register_settings(){
	//Calendar 1 with 3 feeds
	register_setting('fc-settings-group', 'fc_option_feed1');
	register_setting('fc-settings-group', 'fc_option_feed2');
	register_setting('fc-settings-group', 'fc_option_feed3');
	register_setting('fc-settings-group', 'fc_option_calendar');
    register_setting('fc-settings-group', 'defaultview1');

	//Calendar 2 with 3 feeds
	register_setting('fc-settings-group', 'fc2_option_feed1');
	register_setting('fc-settings-group', 'fc2_option_feed2');
	register_setting('fc-settings-group', 'fc2_option_feed3');
	register_setting('fc-settings-group', 'fc2_option_calendar');
    register_setting('fc-settings-group', 'defaultview2');
}
function fc_settings_page(){	
	?>
<div class="wrap">
<h2><?php _e('Full Calendar Js Settings Page') ?></h2>
<h1>Calendar 1</h1>
<form method="post" action="options.php">
    <?php settings_fields( 'fc-settings-group' ); ?>
    <table class="form-table" width="500px">
        <tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc_option_feed1" value="<?php echo get_option('fc_option_feed1'); ?>" /></td> 
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc_option_feed2" value="<?php echo get_option('fc_option_feed2'); ?>" /></td> 
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc_option_feed3" value="<?php echo get_option('fc_option_feed3'); ?>" /></td> 
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Calendar Name', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc_option_calendar" value="<?php echo get_option('fc_option_calendar'); ?>" /></td> 
        </tr>           
        <!--
        <tr>        
        <td>
            <p>Select <b>'default view'</b>:</p>
           <td><input size="130" type="text" name="fc_option_feed1" value="<?php echo get_option('fc_option_feed1'); ?>" /></td> 
        <input type="radio" name="defaultview1" value="monthcal1">Month         
        <input type="radio" name="defaultview1" value="agendaweekcal1">Week         
        <input type="radio" name="defaultview1" value="daycal1">Day
        </td>
        </tr>
        -->

    </table>      
    <table class="form-table" width="500px">
    	<hr style="height: 5px; border: 1px solid #D1D126; background-color: #c00;">
    	<h1>Calendar 2</h1>
    	<tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc2_option_feed1" value="<?php echo get_option('fc2_option_feed1'); ?>" /></td> 
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc2_option_feed2" value="<?php echo get_option('fc2_option_feed2'); ?>" /></td> 
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Calendar XML Feed', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc2_option_feed3" value="<?php echo get_option('fc2_option_feed3'); ?>" /></td> 
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Calendar Name', 'FullCalendarJs') ?></th>
        <td><input size="130" type="text" name="fc2_option_calendar" value="<?php echo get_option('fc2_option_calendar'); ?>" /></td> 
        </tr>
        <!--    
        <tr> 
        <td>
            <p>Select <b>'default view'</b>:</p>

        <input type="radio" name="defaultview2" value="monthcal2">Month         
        <input type="radio" name="defaultview2" value="agendaweekcal2">Week         
        <input type="radio" name="defaultview2" value="daycal2">Day      
        </td>
        </tr>   
        -->
    </table> 

    
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes', 'FullCalendarJs') ?>" /> 
    </p>

</form>
<br/>
 <hr style="height: 5px; border: 1px solid #D1D126; background-color: #c00;">
<h1>Shortcodes/Info</h1>
<p>[fcal id="1"] for calendar 1</p>
<p>or</p>
<p>[fcal id="2"] for calendar 2</p>
<p>Please do not leave the calendar name blank and do not put any white space or spaces into the input fields. Enjoy!</p>
</div>
<?php
}// end fc_settings_page function

//Add Short Codes
function fc_shortcodes($atts){	
	extract(shortcode_atts(array(
		'id' => 1,
		), $atts));
	
	
	if ($id=='1'){
		$cal = get_option('fc_option_calendar');	
	
	}
	if ($id=='2'){
		$cal = get_option('fc2_option_calendar');
	
	} 
	//echo $cal;
	$short = '<div id="'.$cal.'"></div>';	
	
	return $short;
}	

//Register Short Codes
function register_shortcodes(){
add_shortcode('fcal', 'fc_shortcodes');
}
add_action ('init', 'register_shortcodes');
?>