<?php 
    if ( ! defined( 'ABSPATH' ) ) {
        die( '-1' );
    }

    $events_label_singular = tribe_get_event_label_singular();
    $events_label_plural = tribe_get_event_label_plural();

    $event_id = get_the_ID();

?>

<div class="content content-margined">
    <section id="post-<?php echo get_the_ID();?>" class="content-main clearfix container post type-post status-publish format-standard has-post-thumbnail hentry">
        <div class="column span-8">
            <article>
                <div class="post-<?php echo get_the_ID();?> post type-post status-publish format-standard has-post-thumbnail hentry container">
                    <div class="multi-images first-post">
                        <ul>
                            <li><?php the_post_thumbnail();
                                $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                <div class="img-over"> <a class="image-popup" href="<?php echo esc_url($url);?>" title=""> <i class="fa fa-expand"></i> </a></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <article>
                    <ul class="row">
                        <li class="col-sm-3">
                            <div class="date">
                                <span class="day"><?php strtoupper( the_time('D') ); ?></span>
                                <span class="big"><?php the_time('d'); ?></span>
                                <span><?php the_time('F Y'); ?></span>
                            </div>
                        </li>
                        <li class="col-sm-9">
                            <h4><a href="javascript:void(0);" class="tittle"><?php the_title(); ?></a></h4>
                            <?php while ( have_posts() ) :  the_post(); ?>
                                <!-- Event featured image, but exclude link -->

                                <!-- Event content -->
                                <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
                                <div class="tribe-events-single-event-description tribe-events-content entry-content description">
                                    <?php the_content(); ?>
                                </div>
                                <!-- .tribe-events-single-event-description -->

                                <!-- Event meta -->
                                <br/><br/>
                                <div>
                                <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                                <?php tribe_get_template_part( 'modules/meta' ); ?>
                                </div>
                            <?php endwhile; ?>
                        </li>
                    </ul>
                </article>
            </article>
        </div>
        <div class="column span-4 right-bar">
            <?php get_sidebar('etkinlik'); ?>
        </div>
    </section>
</div>