<?php
/**
 * Month View Template
 * The wrapper template for month view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<?php do_action( 'tribe_events_before_template' ) 

?>

<!-- Tribe Bar -->
<?php tribe_get_template_part( 'modules/bar' ); ?>

<!-- Main Events Content -->
<?php tribe_get_template_part( 'month/content' ); ?>

<?php do_action( 'tribe_events_after_template' ) ?>

<?php 
$paged = get_query_var( 'paged' )
		? get_query_var( 'paged' )
		: 1; 
		
	// Build our query, adopt the default number of events to show per page
	$upcoming = new WP_Query( array(
		'post_type' => Tribe__Events__Main::POSTTYPE,
		'posts_per_page' => 100
	) );
if( $upcoming->have_posts() ) :
	while ( $upcoming->have_posts() ) : $upcoming->the_post();
		global $post;
		echo "<pre/>";
		print_r($post);

	endwhile;
endif;
?>
