<?php
/**
 * List View Content Template
 * The content template for the list view. This template is also used for
 * the response that is returned on list view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/content.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<div class="col-md-8">
				<div class="col-md-12" style="margin-bottom:30px;padding:0!important;">
					<?php if ( have_posts() ) : ?>
						<?php do_action( 'tribe_events_before_loop' ); ?>
						<?php tribe_get_template_part( 'list/loop' ) ?>
						<?php do_action( 'tribe_events_after_loop' ); ?>
					<?php endif; ?>
					<?php do_action( 'tribe_events_before_footer' ); ?>
					<div id="tribe-events-footer">
						<?php do_action( 'tribe_events_before_footer_nav' ); ?>
						<?php tribe_get_template_part( 'list/nav', 'footer' ); ?>
						<?php do_action( 'tribe_events_after_footer_nav' ); ?>

					</div>
					<?php do_action( 'tribe_events_after_footer' ) ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="right-bar">
					<?php get_sidebar( 'cocuk' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
