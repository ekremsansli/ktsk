<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings      = array(
	'menu_title' => 'Theme Options',
	'menu_type'  => 'add_menu_page',
	'menu_slug'  => 'cs-framework',
	'ajax_save'  => false,
	);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();



// ----------------------------------------
// general options section                -
// ----------------------------------------
$options[]   = array(
	'name'     => 'general',
	'title'    => 'General',
	'icon'     => 'fa fa-globe',
	'fields'    => array(

		array(
			'id'          => 'show-preloader',
			'type'        => 'switcher',
			'title'       => 'Show preloader',
			'label'   	  => 'Show animation while site is loading',
			'default'	  => true
		),

		array(
			'id'             => 'logo',
			'type'           => 'upload',
			'title'          => 'Website logo (dark)',
			'default' 		 => get_stylesheet_directory_uri() . "/assets/images/logo.png",
			'settings'       => array(
				'button_title' => 'Upload Dark Logo',
				'frame_title'  => 'Choose an image',
				'insert_title' => 'Use this image',
			),
		),

		array(
			'id'             => 'logo-light',
			'type'           => 'upload',
			'title'          => 'Website logo (light)',
			'default' 		 => get_stylesheet_directory_uri() . "/assets/images/logo-white.png",
			'settings'       => array(
				'button_title' => 'Upload Light Logo',
				'frame_title'  => 'Choose an image',
				'insert_title' => 'Use this image',
			),
		),

		array(
			'id'             => 'favicon',
			'type'           => 'upload',
			'title'          => 'Site favicon',
			'default' 		 => get_stylesheet_directory_uri() . "/assets/images/favicon.ico",
			'settings'       => array(
				'button_title' => 'Upload favicon',
				'frame_title'  => 'Choose an image',
				'insert_title' => 'Use this image',
			),
		),

	),
);



// ----------------------------------------
// general options section                -
// ----------------------------------------
$options[]   = array(
	'name'     => 'blog',
	'title'    => 'Blog',
	'icon'     => 'fa fa-globe',
	'fields'    => array(

		array(
			'id'             => 'sidebar-slider',
			'type'           => 'select',
			'title'          => 'Category to show in Sidebar template top slider',
			'options'        => 'categories',
			'class'          => 'chosen',
			'attributes'         => array(
				'data-placeholder' => 'Select category',
				'multiple'         => 'only-key',
			),
		),

		array(
			'id'             => 'imagewall-category',
			'type'           => 'select',
			'title'          => 'Category to show in ImageWall slider',
			'options'        => 'categories',
			'class'          => 'chosen',
			'attributes'         => array(
				'data-placeholder' => 'Select category',
			),
		),
	),
);



// ----------------------------------------
// footer options section                 -
// ----------------------------------------
$options[]   = array(
	'name'     => 'footer',
	'title'    => 'Footer',
	'icon'     => 'fa fa-arrow-down',
	'fields'    => array(

		array(
			'id'          => 'footer-copy-text',
			'type'        => 'textarea',
			'title'       => 'Footer Copyright Text',
			'default'	  => '&copy; Copyright 2015. All Rights Reserved.',
		),

		array(
			'id'          => 'footer-instagram',
			'type'        => 'text',
			'title'       => 'Instagram link',
			'default'	  => 'http://instagram.com/',
		),

		array(
			'id'          => 'footer-inst-gallery',
			'type'        => 'gallery',
			'title'       => 'Instagram gallery',
			'add_title'   => 'Add Images',
			'edit_title'  => 'Edit Images',
			'clear_title' => 'Remove Images',
			'after' => '<p class="cs-text-info">Information: Maximum 8 images. Redundant images will be ignored.</p>',
		),
	),
);



// ----------------------------------------
// Contacts section                 -
// ----------------------------------------
$options[]   = array(
	'name'     => 'contacts',
	'title'    => 'Contact info',
	'icon'     => 'fa fa-phone',
	'fields'    => array(

		array(
			'id'          => 'address',
			'type'        => 'textarea',
			'title'       => 'Address',
			'default'	  => 'Example address',
		),

		array(
			'id'              => 'phones',
			'type'            => 'group',
			'title'           => 'Phones',
			'button_title'    => 'Add Phone Number',
			'accordion_title' => 'New Phone',
			'fields'          => array(

				array(
					'id'          => 'number',
					'type'        => 'text',
					'title'       => 'Number',
				),

				),
			'default'         => array(
				array(
					'number'    => '(123) 345-6789',
				),
			)
		),

		array(
			'id'              => 'emails',
			'type'            => 'group',
			'title'           => 'E-mails',
			'button_title'    => 'Add E-mail',
			'accordion_title' => 'New E-mail',
			'fields'          => array(

				array(
					'id'          => 'email',
					'type'        => 'text',
					'title'       => 'E-mail',
				),
			),

			'default'                     => array(
				array(
					'email'    => 'hello@hello.com',
				),
			)
		),
	),
);



// ----------------------------------------
// social options section                 -
// ----------------------------------------
$options[]   = array(
	'name'     => 'socials',
	'title'    => 'Socials',
	'icon'     => 'fa fa-users',

  // begin: fields
	'fields'    => array(

		array(
			'id'              => 'socials',
			'type'            => 'group',
			'title'           => 'Socials',
			'button_title'    => 'Add New',
			'accordion_title' => 'New Social',
			'fields'          => array(

				array(
					'id'          => 'social-title',
					'type'        => 'text',
					'title'       => 'Social',
					),

				array(
					'id'          => 'social-icon',
					'type'        => 'icon',
					'title'       => 'Icon',
					),

				array(
					'id'          => 'social-link',
					'type'        => 'text',
					'title'       => 'Link',
					),
				),
			'default'                     => array(
				array(
					'social-title'    => 'Facebook',
					'social-icon'     => 'fa fa-facebook',
					'social-link'     => 'http://facebook.com/',
					),
				array(
					'social-title'    => 'Twitter',
					'social-icon'     => 'fa fa-twitter',
					'social-link'     => 'http://twitter.com/',
					),
				)
			),

  ), // end: fields
);



// ----------------------------------------
// team options section                 -
// ----------------------------------------
$options[]   = array(
	'name'     => 'team',
	'title'    => 'Team',
	'icon'     => 'fa fa-users',

  // begin: fields
	'fields'    => array(

		array(
			'id'              => 'team',
			'type'            => 'group',
			'title'           => 'Team',
			'button_title'    => 'Add Staff',
			'accordion_title' => 'New Staff',
			'fields'          => array(

				array(
					'id'          => 'staff-name',
					'type'        => 'text',
					'title'       => 'Name',
				),

				array(
					'id'          => 'staff-photo',
					'type'        => 'upload',
					'title'       => 'Photo',
				),

				array(
					'id'          => 'staff-profession',
					'type'        => 'text',
					'title'       => 'Profession',
				),

				array(
					'id'          => 'staff-desc',
					'type'        => 'textarea',
					'title'       => 'Description',
				),

				array(
					'id'          => 'staff-facebook',
					'type'        => 'text',
					'title'       => 'Facebook link',
				),

				array(
					'id'          => 'staff-twitter',
					'type'        => 'text',
					'title'       => 'Twitter link',
				),

				array(
					'id'          => 'staff-dribbble',
					'type'        => 'text',
					'title'       => 'Dribbble link',
				),

				array(
					'id'          => 'staff-linkedin',
					'type'        => 'text',
					'title'       => 'LinkedIn link',
				),
			),
		),
	), // end: fields
);



// ----------------------------------------
// clients options section                 -
// ----------------------------------------
$options[]   = array(
	'name'     => 'cients',
	'title'    => 'Clients',
	'icon'     => 'fa fa-users',

  // begin: fields
	'fields'    => array(

		array(
			'id'              => 'clients',
			'type'            => 'group',
			'title'           => 'Clients',
			'button_title'    => 'Add Client',
			'accordion_title' => 'New Client',
			'fields'          => array(

				array(
					'id'          => 'client-name',
					'type'        => 'text',
					'title'       => 'Name',
				),

				array(
					'id'          => 'client-image',
					'type'        => 'upload',
					'title'       => 'Image',
				),
			),
		),
	), // end: fields
);



// -----------------------------
// begin: backup option        -
// -----------------------------
$options[] = array(
	'name'     => 'backup_option',
	'title'    => 'Backup',
	'icon'     => 'fa fa-check',
	'fields'   => array(

		array(
			'type' => 'backup',
		),
	),
); // end: backup option




// ------------------------------
// a seperator                  -
// ------------------------------
$options[] = array(
	'name'   => 'seperator_2',
	'title'  => '',
	'icon'   => ''
	);


// ------------------------------
// a seperator                  -
// ------------------------------
$options[] = array(
	'name'   => 'seperator_3',
	'title'  => '',
	'icon'   => ''
	);



CSFramework::instance( $settings, $options );