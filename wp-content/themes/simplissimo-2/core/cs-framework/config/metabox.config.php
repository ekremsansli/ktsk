<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();



// TECHNICAL ARRAYS START

$is_custom_options_active = array( 'page-custom-options', '==', 'true' );

$appearance_options = array(

	/*array(
		'id'        => 'header-type',
		'title'     => 'Header Style',
		'type'      => 'image_select',
		'options'   => array(
			'1' => 'http://dummyimage.com/45x45/2ecc71/fff.png',
			'2' => 'http://dummyimage.com/45x45/3498db/fff.png',
			'3' => 'http://dummyimage.com/45x45/ffbc00/fff.png',
			'4' => 'http://dummyimage.com/45x45/ffbcff/fff.png',
		),
		'radio'     => true,
		'default'	=> '1',
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'enable-header-socials',
		'title'         => 'Show social icons in header',
		'type'          => 'switcher',
		'default'		=> false,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'enable-header-search',
		'title'         => 'Show search in header',
		'type'          => 'switcher',
		'default'		=> false,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'header-search-type',
		'title'         => 'Search type',
		'type'          => 'select',
		'dependency'    => array( 'enable-header-search', '==', 'true' ),
		'options'       => array(
			'full'          => 'Full',
			'short'         => 'Button',
		),
	),

	array(
		'id'        => 'after-header-type',
		'title'     => 'After header style',
		'type'      => 'image_select',
		'options'   => array(
			'1' => 'http://dummyimage.com/45x45/2ecc71/fff.png',
			'2' => 'http://dummyimage.com/45x45/3498db/fff.png',
			'3' => 'http://dummyimage.com/45x45/ffbc00/fff.png',
			'4' => 'http://dummyimage.com/45x45/ffbcff/fff.png',
		),
		'default'	=> '1',
		'radio'     => true,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'        => 'footer-type',
		'title'     => 'Footer Style',
		'type'      => 'image_select',
		'options'   => array(
			'1' => 'http://dummyimage.com/70x70/2ecc71/fff.png',
			'2' => 'http://dummyimage.com/70x70/3498db/fff.png',
		),
		'default'	=> '1',
		'radio'     => true,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'enable-news-ticker',
		'title'         => 'Show news ticker on page',
		'type'          => 'switcher',
		'default'		=> false,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'news-ticker-style',
		'title'         => 'News ticker style',
		'type'          => 'select',
		'options'       => array(
			'1'				=> 'Without title',
			'2'				=> 'With title',
		),
		'default'		=> '1',
		'dependency'    => array( 'enable-news-ticker', '==', 'true' ),
	),

	array(
		'id'            => 'enable-top-slider',
		'title'         => 'Show top slider on page',
		'type'          => 'switcher',
		'default'		=> false,
		'dependency' => $is_custom_options_active,
	),

	array(
		'id'            => 'top-slider-style',
		'title'         => 'Top slider style',
		'type'          => 'select',
		'options'       => array(
			'1'				=> 'Big slides',
			'2'				=> 'Small slides',
		),
		'default'		=> '1',
		'dependency'    => array( 'enable-top-slider', '==', 'true' ),
	),*/
);
// TECHNICAL ARRAYS END



// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------
/*$options[]    = array(
	'id'        => 'simplissimo_custom_page_options',
	'title'     => 'Custom Page Options',
	'post_type' => 'page',
	'context'   => 'normal',
	'priority'  => 'default',
	'sections'  => array(

    // begin: a section
		array(
			'name'  => 'section_1',
			'title' => 'Section 1',
			'icon'  => 'fa fa-cog',

      // begin: fields
			'fields' => array(

        // begin: a field
				array(
					'id'    => 'section_1_text',
					'type'  => 'text',
					'title' => 'Text Field',
					),
        // end: a field

				array(
					'id'    => 'section_1_textarea',
					'type'  => 'textarea',
					'title' => 'Textarea Field',
					),

				array(
					'id'    => 'section_1_upload',
					'type'  => 'upload',
					'title' => 'Upload Field',
					),

				array(
					'id'    => 'section_1_switcher',
					'type'  => 'switcher',
					'title' => 'Switcher Field',
					'label' => 'Yes, Please do it.',
					),

      ), // end: fields
    ), // end: a section

    // begin: a section
		array(
			'name'  => 'section_2',
			'title' => 'Section 2',
			'icon'  => 'fa fa-tint',
			'fields' => array(

				array(
					'id'      => 'section_2_color_picker_1',
					'type'    => 'color_picker',
					'title'   => 'Color Picker 1',
					'default' => '#2ecc71',
					),

				array(
					'id'      => 'section_2_color_picker_2',
					'type'    => 'color_picker',
					'title'   => 'Color Picker 2',
					'default' => '#3498db',
					),

				array(
					'id'      => 'section_2_color_picker_3',
					'type'    => 'color_picker',
					'title'   => 'Color Picker 3',
					'default' => '#9b59b6',
					),

				array(
					'id'      => 'section_2_color_picker_4',
					'type'    => 'color_picker',
					'title'   => 'Color Picker 4',
					'default' => '#34495e',
					),

				array(
					'id'      => 'section_2_color_picker_5',
					'type'    => 'color_picker',
					'title'   => 'Color Picker 5',
					'default' => '#e67e22',
					),

				),
			),
    // end: a section

		),
);*/



// -----------------------------------------
// Page Side Metabox Options               -
// -----------------------------------------
$options[]    = array(
	'id'        => 'simplissimo_custom_page_side_options',
	'title'     => 'Custom Page Side Options',
	'post_type' => 'page',
	'context'   => 'side',
	'priority'  => 'default',
	'sections'  => array(

		array(
			'name'   => 'section_page',
			'fields' => array(

				array(
					'id'            => 'header-type',
					'title'         => 'Header style',
					'type'          => 'select',
					'options'       => array(
						'1'				=> 'Light',
						'2'				=> 'Dark',
					),
					'default'		=> '1',
				),
			),
		),

	),
);



// -----------------------------------------
// Post Metabox Options                    -
// -----------------------------------------
$options[]    = array(
	'id'        => 'simplissimo_custom_post_options',
	'title'     => 'Post Style Options',
	'post_type' => 'post',
	'context'   => 'side',
	'priority'  => 'default',
	'sections'  => array(

		array(
			'name'   => 'options',
			'fields' => array(

				array(
					'id'		=> 'gallery-type',
					'title'		=> 'Gallery type',
					'type'		=> 'image_select',
					'options'	=> array(
						'1' => 'http://dummyimage.com/60x60/18228f/fff.png&text=Simple',
						'2' => 'http://dummyimage.com/60x60/29b355/fff.png&text=Thumbs',
						'3' => 'http://dummyimage.com/60x60/bf8d20/fff.png&text=Carousel',
					),
					'default'	=> 1,
					'before' => '<p class="cs-text-info">Information: This option is active only if selected post format is "Gallery".</p> ',
				),
			),
		),
	),
);

CSFramework_Metabox::instance( $options );