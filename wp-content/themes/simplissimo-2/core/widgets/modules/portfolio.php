<?php  /**
 * Portfolio Widget
 *
 * This file is used to register and display the Layers - Portfolio widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( !class_exists( 'Miresume_Portfolio_Widget' ) ) {
	class Miresume_Portfolio_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function Miresume_Portfolio_Widget(){

			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			global $textdomain;
			$this->widget_title = __( 'Portfolio' , $textdomain );
			$this->widget_id = 'portfolio';
			$this->post_type = 'portfolio';
			$this->taxonomy = 'portfolio-category';
			$this->checkboxes = array(
					/*'test'*/
				); // @TODO: Try make this more dynamic, or leave a different note reminding users to change this if they add/remove checkboxes

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain ) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => 'Latest Portfolio Items',
				'excerpt' => 'Stay up to date with all our latest news and launches. Only the best quality makes it onto our blog!',
				'portfolio-category' => 0,
				'posts_per_page' => 6,
				'columns_full' => 3,
				'columns_mobile' => 2,
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$widget = wp_parse_args( $instance, $instance_defaults );

			// Set the span class for each column
			$widths = array( 1 => '12',2 => '6',3 => '4',4 => '3',6 => '2' );
			$col_class = 'col-sm-' . $widths[$widget['columns_full']] . ' col-xs-' . $widths[$widget['columns_mobile']];

			// Set the background & font styling
			$back_options = array();
			if( !empty( $widget[ 'background_image' ] ) ) $back_options['image'] = $widget[ 'background_image' ];
			if( !empty( $widget[ 'background_color' ] ) ) $back_options['color'] = $widget[ 'background_color' ];

			if( !empty( $back_options ) ) layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $back_options ) );

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );

			// Begin query arguments
			$query_args = array();
			$query_args[ 'paged' ] = (get_query_var('page')) ? get_query_var('page') : 1;
			$query_args[ 'post_type' ] = $this->post_type;
			$query_args[ 'posts_per_page' ] = $widget['posts_per_page'];
			if( isset( $widget['order'] ) ) {

				$decode_order = json_decode( $widget['order'], true );

				if( is_array( $decode_order ) ) {
					foreach( $decode_order as $key => $value ){
						$query_args[ $key ] = $value;
					}
				}
			}

			// Do the special taxonomy array()
			if( isset( $widget['portfolio-category'] ) && '' != $widget['portfolio-category'] && 0 != $widget['portfolio-category'] ){
				$query_args['tax_query'] = array(
					array(
						"taxonomy" => $this->taxonomy,
						"field" => "id",
						"terms" => $widget['portfolio-category']
					)
				);
			} elseif( !isset( $widget['hide_category_filter'] ) ) {
				$terms = get_terms( $this->taxonomy );
			} // if we haven't selected which category to show, let's load the $terms for use in the filter

			// Do the WP_Query
			$post_query = new WP_Query( $query_args );

			// Set the meta to display
			global $layers_post_meta_to_display;
			$layers_post_meta_to_display = array(); ?>

			<!-- Portfolio Section Start -->
			<div id="<?php echo esc_attr($widget_id); ?>" class="portfolio-main <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>">
				<div class="container">
					<?php if ( !empty( $widget['title'] ) || !empty( $widget['excerpt'] ) ) { ?><div class="title" style="visibility: visible;">
						<?php if ( !empty( $widget['title'] ) ) { ?><h2><?php echo esc_html( $widget['title'] ); ?></h2><?php } ?>
						<?php if ( !empty( $widget['excerpt'] ) ) { ?><p><?php echo esc_html( $widget['excerpt'] ); ?></p><?php } ?>
					</div><?php } ?>
					<div class="gallery-block bounceInRight" id="portfolio" style="visibility: visible;">
						<ul class="row">
<?php 					if( $post_query->have_posts() ) {
							while( $post_query->have_posts() ) {
								$post_query->the_post();
								global $post;
								if ( has_post_thumbnail( $post->ID ) ) {
									$post_thumb_small = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
									$post_thumb_big = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
									$post_thumb_small = $post_thumb_small[0];
									$post_thumb_big = $post_thumb_big[0];
								} else {
									continue;
								}
?>
							<li class="<?php echo $col_class; ?>"><a href="<?php echo $post_thumb_big; ?>"><img src="<?php echo $post_thumb_big; ?>" alt=""><div class="overlay"><span><?php echo $post->post_title; ?></span></div></a></li>
								<?php }; // while have_posts ?>
							<?php }; // if have_posts ?>
						</ul>
					</div>
				</div>
			</div>
			<!-- Portfolio Section End -->

			<?php // Reset WP_Query
				wp_reset_postdata();
			?>
		<?php }

		/**
		*  Widget update
		*/

		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );

			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
					'columns' => array(
						'icon-css' => 'icon-columns',
						'label' => __( 'Columns', 'layerswp' ),
						'elements' => array(
							'columns_full' => array(
								'type' => 'select',
								'label' => __( 'Columns (for tablets & bigger)', 'layerswp' ),
								'name' => $this->get_field_name( 'columns_full' ),
								'id' => $this->get_field_id( 'columns_full' ),
								'value' => ( isset( $columns_full ) ) ? $columns_full : NULL,
								'options' => array(
									'1' => __( '1 Column', 'layerswp' ),
									'2' => __( '2 Columns', 'layerswp' ),
									'3' => __( '3 Columns', 'layerswp' ),
									'4' => __( '4 Columns', 'layerswp' ),
									'6' => __( '6 Columns', 'layerswp' )
								),
							),
							'columns_mobile' => array(
								'type' => 'select',
								'label' => __( 'Columns (for mobiles)', 'layerswp' ),
								'name' => $this->get_field_name( 'columns_mobile' ),
								'id' => $this->get_field_id( 'columns_mobile' ),
								'value' => ( isset( $columns_mobile ) ) ? $columns_mobile : NULL,
								'options' => array(
									'1' => __( '1 Column', 'layerswp' ),
									'2' => __( '2 Columns', 'layerswp' ),
									'3' => __( '3 Columns', 'layerswp' ),
									'4' => __( '4 Columns', 'layerswp' ),
									'6' => __( '6 Columns', 'layerswp' )
								),
							),
						)
					),
					'background' => array(
						'icon-css' => 'icon-photo',
						'label' => __( 'Background', 'layerswp' ),
						'elements' => array(
							'background_image' => array(
								'type' => 'image',
								'label' => __( 'Background Image', 'layerswp' ),
								'button_label' => __( 'Choose Image', 'layerswp' ),
								'name' => $this->get_field_name( 'background_image' ),
								'id' => $this->get_field_id( 'background_image' ),
								'value' => ( isset( $background_image ) ) ? $background_image : NULL
							),
							'background_color' => array(
								'type' => 'color',
								'label' => __( 'Color', 'layerswp' ),
								'name' => $this->get_field_name( 'background_color' ),
								'id' => $this->get_field_id( 'background_color' ),
								'value' => ( isset( $background_color ) ) ? $background_color : NULL
							),
						)
					),
				) );

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'design' ),
					'id' => $this->get_field_id( 'design' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>
			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' =>  __( 'Portfolio' , 'layerswp' ),
					'icon_class' =>'post'
				) ); ?>

				<section class="layers-accordion-section layers-content">

					<div class="layers-row layers-push-bottom">
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>

						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'excerpt' ) ,
									'id' => $this->get_field_id( 'excerpt' ) ,
									'placeholder' => __( 'Short Excerpt' , 'layerswp' ),
									'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
									'class' => 'layers-textarea layers-large'
								)
							); ?>
						</p>
						<?php // Grab the terms as an array and loop 'em to generate the $options for the input
						$terms = get_terms( $this->taxonomy );
						if( !is_wp_error( $terms ) ) { ?>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php echo __( 'Category to Display' , 'layerswp' ); ?></label>
								<?php $category_options[ 0 ] = __( 'All' , 'layerswp' );
								foreach ( $terms as $t ) $category_options[ $t->term_id ] = $t->name;
								echo $this->form_elements()->input(
									array(
										'type' => 'select',
										'name' => $this->get_field_name( 'category' ) ,
										'id' => $this->get_field_id( 'category' ) ,
										'placeholder' => __( 'Select a Category' , 'layerswp' ),
										'value' => ( isset( $category ) ) ? $category : NULL ,
										'options' => $category_options
									)
								); ?>
							</p>
						<?php } // if !is_wp_error ?>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>"><?php echo __( 'Number of items to show' , 'layerswp' ); ?></label>
							<?php $select_options[ '-1' ] = __( 'Show All' , 'layerswp' );
							$select_options = $this->form_elements()->get_incremental_options( $select_options , 1 , 20 , 1);
							echo $this->form_elements()->input(
								array(
									'type' => 'number',
									'name' => $this->get_field_name( 'posts_per_page' ) ,
									'id' => $this->get_field_id( 'posts_per_page' ) ,
									'value' => ( isset( $posts_per_page ) ) ? $posts_per_page : NULL ,
									'min' => '-1',
									'max' => '100'
								)
							); ?>
						</p>

						<p class="layers-form-item">
							<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php echo __( 'Sort by' , 'layerswp' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'select',
									'name' => $this->get_field_name( 'order' ) ,
									'id' => $this->get_field_id( 'order' ) ,
									'value' => ( isset( $order ) ) ? $order : NULL ,
									'options' => $this->form_elements()->get_sort_options()
								)
							); ?>
						</p>
					</div>
				</section>

			</div>
		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	register_widget("Miresume_Portfolio_Widget");
}