<?php  /**
 * Slider Widget
 *
 * This file is used to register and display the Layers - Slider widget.
 *
 * @package MiResume
 * @since MiResume 1.0
 */
if( !class_exists( 'Miresume_Slider_Widget' ) ) {
	class Miresume_Slider_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function Miresume_Slider_Widget(){

			/**
			* Widget variables
			*
			* @param    string          $widget_title       Widget title
			* @param    string          $widget_id          Widget slug for use as an ID/classname
			* @param    string          $post_type          (optional) Post type for use in widget options
			* @param    string          $taxonomy           (optional) Taxonomy slug for use as an ID/classname
			* @param    array           $checkboxes     (optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			global $textdomain;
			$this->widget_title = __( 'Slider' , $textdomain );
			$this->widget_id = 'slider';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array(
					/*'checkbox',*/
				);

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'slide_ids' => rand( 1 , 1000 ).','.rand( 1 , 1000 ).','.rand( 1 , 1000 )
			);

			$this->slide_defaults = array (
				'title' => 'New slide',
				'excerpt' => 'Slide description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, labore, itaque. Pariatur, nemo reiciendis. Tenetur aut dolores, magnam, fugit ipsa, nisi molestias mollitia voluptates illo iste dolorem nihil explicabo, perspiciatis.',
				'itemscount' => 1,
				'slidetype' => 1,
				'item_number' => 75,
			);

			// Setup the defaults for each slide object
			foreach( explode( ',', $this->defaults[ 'slide_ids' ] ) as $slide_id ) {
					$this->defaults[ 'slides' ][ $slide_id ] = $this->slide_defaults;
			}
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			global $wp_customize;

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			$widget = wp_parse_args( $instance , $instance_defaults );

			// Set the background & font styling
			$back_options = array();
			if( !empty( $widget[ 'background_image' ] ) ) $back_options['image'] = $widget[ 'background_image' ];
			if( !empty( $widget[ 'background_color' ] ) ) $back_options['color'] = $widget[ 'background_color' ];

			if( !empty( $back_options ) ) layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $back_options ) );

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );
			?>

			<?php if( !empty( $widget['slides'] ) ) { ?>
				<div id="<?php echo esc_attr($widget_id); ?>" class="my_story_block <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?> <?php echo $this->get_widget_layout_class( $widget ); ?>">
					<div class="container">
						<div class="carousel-slider">
							<div class="owl-carousel owl-example">
								<?php // Set total width so that we can apply .last to the final container
								$total_width = 0; ?>
								<?php foreach ( explode( ',', $widget[ 'slide_ids' ] ) as $slide_key ) {

									// Make sure we've got a slide going on here
									if( !isset( $widget[ 'slides' ][ $slide_key ] ) ) continue;

									// Setup the relevant slide
									$slide = $widget[ 'slides' ][ $slide_key ];

									/*// Set the background styling
									if( !empty( $slide['design'][ 'background' ] ) ) layers_inline_styles( '#' . $widget_id . '-' . $slide_key , 'background', array( 'background' => $slide['design'][ 'background' ] ) );
									if( !empty( $slide['design']['fonts'][ 'color' ] ) ) layers_inline_styles( '#' . $widget_id . '-' . $slide_key , 'color', array( 'selectors' => array( 'h5.heading a', 'h5.heading' , 'div.excerpt' , 'div.excerpt p' ) , 'color' => $slide['design']['fonts'][ 'color' ] ) );
									if( !empty( $slide['design']['fonts'][ 'shadow' ] ) ) layers_inline_styles( '#' . $widget_id . '-' . $slide_key , 'text-shadow', array( 'selectors' => array( 'h5.heading a', 'h5.heading' , 'div.excerpt' , 'div.excerpt p' )  , 'text-shadow' => $slide['design']['fonts'][ 'shadow' ] ) );

									if( !isset( $slide[ 'width' ] ) ) $slide[ 'width' ] = $this->slide_defaults[ 'width' ];
									// Add the correct span class
									$span_class = 'span-' . $slide[ 'width' ];

									// Add .last to the final slide
									$total_width += $slide[ 'width' ];

									if( 12 == $total_width ) {
										$span_class .= ' last';
										$total_width = 0;
									} elseif( $total_width > 12 ) {
										$total_width = 0;
									}

									// Set Featured Media
									$featureimage = $this->check_and_return( $slide , 'design' , 'featuredimage' );
									$featurevideo = $this->check_and_return( $slide , 'design' , 'featuredvideo' );

									// Set Image Sizes
									if( isset( $slide['design'][ 'imageratios' ] ) ){

										// Translate Image Ratio into something usable
										$image_ratio = layers_translate_image_ratios( $slide['design'][ 'imageratios' ] );

										if( !isset( $slide[ 'width' ] ) ) $slide[ 'width' ] = 6;

										if( 4 > $slide['width'] ){
											$use_image_ratio = $image_ratio . '-medium';
										} else {
											$use_image_ratio = $image_ratio . '-large';
										}

									} else {
										if( 4 > $slide['width'] ){
											$use_image_ratio = 'medium';
										} else {
											$use_image_ratio = 'full';
										}
									}

									$media = layers_get_feature_media(
										$featureimage ,
										$use_image_ratio ,
										$featurevideo
									);

									// Set the slide link
									$link = $this->check_and_return( $slide , 'link' );

									// Set Slide CSS Classes
									$slide_class = array();
									$slide_class[] = 'layers-masonry-slide';
									$slide_class[] = $span_class;
									if( !isset( $widget['design'][ 'gutter' ] ) ) {
										$slide_class[] = 'slide-flush';
									} else {
										$slide_class[] = 'slide';
									}
									if( '' != $this->check_and_return( $slide, 'design' , 'background', 'image' ) || '' != $this->check_and_return( $slide, 'design' , 'background', 'color' ) ) {
										$slide_class[] = 'content';
									}
									if( false != $media ) {
										$slide_class[] = 'has-image';
									}
									$slide_class = implode( ' ', $slide_class );

									// Set Slide Inner CSS Classes
									$slide_inner_class = array();
									$slide_inner_class[] = 'media';
									if( !$this->check_and_return( $widget, 'design', 'gutter' ) ) {
										$slide_inner_class[] = 'no-push-bottom';
									}
									$slide_inner_class[] = $this->check_and_return( $slide, 'design', 'imagealign' );
									$slide_inner_class[] = $this->check_and_return( $slide, 'design', 'fonts' , 'size' );
									$slide_inner_class = implode( ' ', $slide_inner_class );*/ ?>

									<?php
										$slides_classes = array( 'simple', 'expertise_block', 'work_exp', 'award_block', 'education_block', 'tech_skills' );
										$slides_comments = array( 'Objective', 'Expertise', 'Work Experiance', 'Award', 'Education', 'Technical Skills' );
									?>
									<div class="item"><!-- <?php echo esc_attr( $slides_comments[$slide["slidetype"]] ); ?> block start -->
										<div class="<?php echo esc_attr( $slides_classes[$slide["slidetype"]] ); ?>">
											<?php if ( $slide['title'] || $slide['excerpt'] ) { ?>
											<div class="title">
												<?php if ( $slide['title'] != '' ) { echo '<h2>' . $slide['title'] . '</h2>'; } ?>
												<?php if ( $slide['excerpt'] != '' ) { echo wpautop( $slide['excerpt'] ); } ?>
											</div>
											<?php } ?>

									<?php
										if ( $slide['slidetype'] == 1 ) { // if SlideType == Features ?>
											<div class="col-lg-12 expertise_block_sub" style="visibility: visible;">
												<?php
													for ($i=1; $i <= $slide['itemscount']; $i++) {
														$icon_name = 'icon_' . $i;
														$title_name = 'title_' . $i;
														$excerpt_name = 'excerpt_' . $i;

														if ( $slide[$icon_name] != '' || $slide[$title_name] != '' || $slide[$excerpt_name] != '' ) { ?>
															<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 expertise_box">
																<div class="icon_box col-lg-3 col-md-3 col-sm-4"><span class="<?php echo esc_attr( $slide[$icon_name] ); ?>"></span></div>
																<div class="details_bar col-lg-9 col-md-9 col-sm-8">
																	<h2><?php echo esc_textarea( $slide[$title_name] ); ?></h2>
																	<?php print wpautop( $slide[$excerpt_name] ); ?>
																</div>
															</div>
													<?php	}
													}
												?>
											</div>
										<?php } elseif ( $slide['slidetype'] == 2 ) { // if SlideType == Accordion ?>
											<div class="accordion accordion-bg clearfix">
											<?php
												for ($i=1; $i <= $slide['itemscount']; $i++) {
													$title_name = 'title_' . $i;
													$excerpt_name = 'excerpt_' . $i;

													if ( $slide[$title_name] != '' || $slide[$excerpt_name] != '' ) { ?>
														<div class="acctitle"><?php echo esc_textarea( $slide[$title_name] ); ?><i class="fa fa-chevron-up"></i><i class="fa fa-chevron-down"></i></div>
														<div class="acc_content clearfix"><?php echo esc_textarea( $slide[$excerpt_name] ); ?></div>
												<?php	}
												}
											?>
											</div>
										<?php } elseif ( $slide['slidetype'] == 3 ) { // if SlideType == Awards ?>
											<div class="award_block_sub">
											<?php
												for ($i=1; $i <= $slide['itemscount']; $i++) {
													$icon_name = 'icon_' . $i;
													$excerpt_name = 'excerpt_' . $i;

													if ( $slide[$icon_name] != '' || $slide[$excerpt_name] != '' ) { ?>
														<div class="award_box">
															<div class="aw-icon"><span class="<?php echo esc_attr( $slide[$icon_name] ); ?>"></span></div>
															<div class="text">
																<?php print wpautop( $slide[$excerpt_name] ); ?>
															</div>
														</div>
												<?php	}
												}
											?>
											</div>
										<?php } elseif ( $slide['slidetype'] == 4 ) { // if SlideType == Education ?>
											<div class="education_block_sub">
											<?php
												for ($i=1; $i <= $slide['itemscount']; $i++) {
													$title_name = 'title_' . $i;
													$subtitle_name = 'subtitle_' . $i;
													$excerpt_name = 'excerpt_' . $i;

													if ( $slide[$subtitle_name] != '' || $slide[$title_name] != '' || $slide[$excerpt_name] != '' ) { ?>
														<div class="education_row">
															<div class="left_block col-lg-6 col-sm-6 col-md-6 col-xs-12">
																<div class="left-details">
																	<h2><?php echo esc_textarea( $slide[$title_name] ); ?></h2>
																	<p><?php echo esc_textarea( $slide[$subtitle_name] ); ?></p>
																</div>
															</div>
															<div class="right_block col-lg-6 col-sm-6 col-md-6 col-xs-12">
																<?php print wpautop( $slide[$excerpt_name] ); ?>
															</div>
														</div>
												<?php	}
												}
											?>
											 </div>
										<?php } elseif ( $slide['slidetype'] == 5 ) { // if SlideType == Skills ?>
											<div class="skill-block">
												<ul class="row">
												<?php
													for ($i=1; $i <= $slide['itemscount']; $i++) {
														$title_name = 'title_' . $i;
														$number_name = 'number_' . $i;
														$excerpt_name = 'excerpt_' . $i;

														if ( $slide[$number_name] != '' || $slide[$title_name] != '' || $slide[$excerpt_name] != '' ) { ?>
															<li class="col-sm-3 col-xs-3">
																<div class="skill-circle"><span id="timer<?php echo $i; ?>" data-counter="<?php echo esc_textarea( $slide[$number_name] ); ?>"></span></div>
																<h3><?php echo esc_textarea( $slide[$title_name] ); ?></h3>
																<?php print wpautop( $slide[$excerpt_name] ); ?>
															</li>
													<?php	}
													}
												?>
												</ul>
											</div>
										<?php } ?>

									</div><!-- <?php echo esc_attr( $slides_comments[$slide["slidetype"]] ); ?> block end -->
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php }

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
					'background' => array(
						'icon-css' => 'icon-photo',
						'label' => __( 'Background', 'layerswp' ),
						'elements' => array(
							'background_image' => array(
								'type' => 'image',
								'label' => __( 'Background Image', 'layerswp' ),
								'button_label' => __( 'Choose Image', 'layerswp' ),
								'name' => $this->get_field_name( 'background_image' ),
								'id' => $this->get_field_id( 'background_image' ),
								'value' => ( isset( $background_image ) ) ? $background_image : NULL
							),
							'background_color' => array(
								'type' => 'color',
								'label' => __( 'Color', 'layerswp' ),
								'name' => $this->get_field_name( 'background_color' ),
								'id' => $this->get_field_id( 'background_color' ),
								'value' => ( isset( $background_color ) ) ? $background_color : NULL
							),
						)
					),
				)
			);

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'slider' ),
					'id' => $this->get_field_id( 'slider' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'Slider' , 'layerswp' ),
					'icon_class' =>'location'
				) ); ?>

				<section class="layers-accordion-section layers-content">
					<?php echo $this->form_elements()->input(
						array(
							'type' => 'hidden',
							'name' => $this->get_field_name( 'slide_ids' ) ,
							'id' => 'slide_ids_input_' . $this->number,
							'value' => ( isset( $slide_ids ) ) ? $slide_ids : NULL
						)
					); ?>

					<?php // If we have some slides, let's break out their IDs into an array
					if( isset( $slide_ids ) && '' != $slide_ids ) $slides = explode( ',' , $slide_ids ); ?>

					<ul id="slide_list_<?php echo esc_attr( $this->number ); ?>" class="layers-accordions layers-accordions-sortable layers-sortable" data-id_base="<?php echo $this->id_base; ?>" data-number="<?php echo $this->number; ?>">
						<?php if( isset( $slides ) && is_array( $slides ) ) { ?>
							<?php foreach( $slides as $slideguid ) {
								$this->slide_item( array(
											'id_base' => $this->id_base ,
											'number' => $this->number
										) ,
										$slideguid ,
										( isset( $instance[ 'slides' ][ $slideguid ] ) ) ? $instance[ 'slides' ][ $slideguid ] : NULL );
							} ?>
						<?php }?>
					</ul>
					<button class="layers-button btn-full layers-add-widget-slide add-new-widget" data-number="<?php echo esc_attr( $this->number ); ?>"><?php _e( 'Add New Slide' , 'layerswp' ) ; ?></button>

						<!-- <p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>
						<div class="layers-row" style="margin-bottom: 20px;">
							<p class="layers-form-item layers-slide layers-span-6">
								<label for="<?php echo $this->get_field_id( 'download_icon' ); ?>"><?php _e( 'Download Icon' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'text',
										'name' => $this->get_field_name( 'download_icon' ),
										'id' => $this->get_field_id( 'download_icon' ),
										'placeholder' => __( 'icon-download' , 'layerswp' ),
										'value' => ( isset( $download_icon ) ) ? trim($download_icon, '.') : NULL ,
										'class' => 'layers-text',
									)
								); ?>
								<small class="layers-small-note">
									<?php _e( 'Full list of icons you can find here: <a href="http://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website#codes" target="_blank">ET-Line Font</a>. You have copy-paste only classname, for example "icon-document".', 'layerswp' ); ?>
								</small>
							</p>
							<p class="layers-form-item layers-slide layers-span-6">
								<label for="<?php echo $this->get_field_id( 'resume_link' ); ?>"><?php _e( 'Resume Link' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'text',
										'name' => $this->get_field_name( 'resume_link' ),
										'id' => $this->get_field_id( 'resume_link' ),
										'placeholder' => __( 'http://' , 'layerswp' ),
										'value' => ( isset( $resume_link ) ) ? $resume_link : NULL ,
										'class' => 'layers-text',
									)
								); ?>
							</p>
						</div>
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'excerpt' ) ,
									'id' => $this->get_field_id( 'excerpt' ) ,
									'placeholder' =>  __( 'Short Excerpt' , 'layerswp' ),
									'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
									'class' => 'layers-textarea layers-large',
								)
							); ?>
						</p> -->
				</section>
			</div>

		<?php } // Form
		function slide_item( $widget_details = array() , $slide_guid = NULL , $instance = NULL ){

			// Extract Instance if it's there so that we can use the values in our inputs

			// $instance Defaults
			$instance_defaults = $this->slide_defaults;

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			// If there is no GUID create one. There should always be one but this is a fallback
			if( ! isset( $slide_guid ) ) $slide_guid = rand( 1 , 1000 );

			// Turn the widget details into an object, it makes the code cleaner
			$widget_details = (object) $widget_details;

			// Set a count for each row
			if( !isset( $this->slide_item_count ) ) {
				$this->slide_item_count = 0;
			} else {
				$this->slide_item_count++;
			} ?>

			<?php
				$slidetype_options = array();
				$slidetype_options[] = __( 'Title with text' , 'layerswp' );
				$slidetype_options[] = __( 'Features' , 'layerswp' );
				$slidetype_options[] = __( 'Accordion' , 'layerswp' );
				$slidetype_options[] = __( 'Awards' , 'layerswp' );
				$slidetype_options[] = __( 'Education' , 'layerswp' );
				$slidetype_options[] = __( 'Skills' , 'layerswp' );
			?>

				<li class="layers-accordion-item" data-guid="<?php echo esc_attr( $slide_guid ); ?>">
					<a class="layers-accordion-title">
						<span>
							<?php _e( 'Slide' , 'layerswp' ); ?>
							<span class="layers-detail">
								<?php echo ( isset( $title ) ? ': ' . substr( stripslashes( strip_tags($title ) ), 0 , 50 ) : NULL ); ?>
								<?php echo ( isset( $title ) && strlen( $title ) > 50 ? '...' : NULL ); ?>
							</span>
						</span>
					</a>
					<section class="layers-accordion-section layers-content">
						<?php $this->design_bar(
							'top', // CSS Class Name
							array(
								'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, 'design' ),
								'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'design' ),
								'number' => $widget_details->number,
								'show_trash' => true
							), // Widget Object
							$instance, // Widget Values
							array(
								/*'background',
								'featuredimage',
								'imagealign',
								'fonts',*/
								'custom',
							), // Standard Components
							array(
								'type' => array(
									'icon-css' => 'icon-display',
									'label' => 'Slide Type',
									'elements' => array(
										'layout' => array(
											'type' => 'select',
											'label' => __( '' , 'layerswp' ),
											'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, 'slidetype' ),
											'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'slidetype' ),
											'value' => ( isset( $slidetype ) ) ? $slidetype : 1 ,
											'class' => 'type_select',
											'options' => $slidetype_options
											/*array(
												'1' => __( '1 of 12 slides' , 'layerswp' ),
												'2' => __( '2 of 12 slides' , 'layerswp' ),
												'3' => __( '3 of 12 slides' , 'layerswp' ),
												'4' => __( '4 of 12 slides' , 'layerswp' ),
												'5' => __( '5 of 12 slides' , 'layerswp' ),
												'6' => __( '6 of 12 slides' , 'layerswp' ),
												'8' => __( '8 of 12 slides' , 'layerswp' ),
												'9' => __( '9 of 12 slides' , 'layerswp' ),
												'10' => __( '10 of 12 slides' , 'layerswp' ),
												'12' => __( '12 of 12 slides' , 'layerswp' )
											)*/
										)
									)
								),
							)
						); ?>

						<div class="layers-row">
							<p class="layers-form-item">
								<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'title' ); ?>"><?php _e( 'Title' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'text',
										'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, 'title' ),
										'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'title' ),
										'placeholder' => __( 'Enter title here' , 'layerswp' ),
										'value' => ( isset( $title ) ) ? $title : NULL ,
										'class' => 'layers-text layers_title'
									)
								); ?>
							</p>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'excerpt' ); ?>"><?php _e( 'Excerpt' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'textarea',
										'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, 'excerpt' ),
										'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'excerpt' ),
										'placeholder' => __( 'Short Excerpt' , 'layerswp' ),
										'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
										'class' => 'layers-form-item layers-textarea layers_description',
										'rows' => 5
									)
								); ?>
							</p>
							<?php
								$hidden_attr = ( $slidetype == '0' ) ? ' hidden' : '';
								/*switch ($slidetype) {
									case 'value':
										# code...
										break;
									
									default:
										$max_items = 6;
										break;
								}*/
							?>
							<p class="layers-form-item custom_counter<?php echo $hidden_attr; ?>">
								<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'itemscount' ); ?>"><?php _e( 'Items' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'number',
										'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, 'itemscount' ),
										'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, 'itemscount' ),
										/*'placeholder' => __( 'Select slide type' , 'layerswp' ),*/
										'class' => 'layers-select select_items_count',
										'min' => 1,
										'max' => 6,
										'step' => 1,
										'value' => ( isset( $itemscount ) ) ? $itemscount : NULL ,
									)
								); ?>
							</p>
							<div class="custom_items_list<?php echo $hidden_attr; ?>">
							<?php $current_items = ( $itemscount ) ? $itemscount : 1;

							for ($i=1; $i < 7; $i++) {
								$icon_name = 'icon_' . $i;
								$title_name = 'title_' . $i;
								$subtitle_name = 'subtitle_' . $i;
								$number_name = 'number_' . $i;
								$excerpt_name = 'excerpt_' . $i;

								// var_dump($slidetype);
								$icon_vis = $title_vis = $subtitle_vis = $number_vis = $excerpt_vis = ' hidden';

								switch ( $slidetype ) {
									case 1: // SlideType == Features
										$icon_vis = $title_vis = $excerpt_vis = '';
										break;

									case 2: // SlideType == Accordion
										$title_vis = $excerpt_vis = '';
										break;

									case 3: // SlideType == Awards
										$icon_vis = $excerpt_vis = '';
										break;

									case 4: // SlideType == Education
										$title_vis = $subtitle_vis = $excerpt_vis = '';
										break;

									default: // SlideType == Skills
										$title_vis = $number_vis = $excerpt_vis = '';
										break;
								}
								$hidden_row = ( $i <= $current_items ) ? '' : ' hidden';
								?>
								<div class="layers-row bordered<?php echo esc_attr( $hidden_row ); ?>">
									<span class="item_counter">Item #<?php echo $i; ?></span>
									<div class="bordered_nested">
										<p class="layers-form-item<?php echo esc_attr($title_vis); ?>">
											<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $title_name ); ?>"><?php _e( 'Item title' , 'layerswp' ); ?></label>
											<?php echo $this->form_elements()->input(
												array(
													'type' => 'text',
													'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, $title_name ),
													'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $title_name ),
													'placeholder' => __( 'Enter title here' , 'layerswp' ),
													'value' => ( isset( $$title_name ) ) ? $$title_name : NULL ,
													'class' => 'layers-text custom_js_title'
												)
											); ?>
										</p>
										<p class="layers-form-item<?php echo esc_attr($subtitle_vis); ?>">
											<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $subtitle_name ); ?>"><?php _e( 'Item subtitle' , 'layerswp' ); ?></label>
											<?php echo $this->form_elements()->input(
												array(
													'type' => 'text',
													'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, $subtitle_name ),
													'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $subtitle_name ),
													'placeholder' => __( 'Enter subtitle here' , 'layerswp' ),
													'value' => ( isset( $$subtitle_name ) ) ? $$subtitle_name : NULL ,
													'class' => 'layers-text custom_js_subtitle'
												)
											); ?>
										</p>
										<p class="layers-form-item<?php echo esc_attr($icon_vis); ?>">
											<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $icon_name ); ?>"><?php _e( 'Item icon' , 'layerswp' ); ?></label>
											<?php echo $this->form_elements()->input(
												array(
													'type' => 'text',
													'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, $icon_name ),
													'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $icon_name ),
													'value' => ( isset( $$icon_name ) ) ? $$icon_name : NULL ,
													'class' => 'layers-text custom_js_icon',
												)
											); ?>
											<small class="layers-small-note">
												<?php _e( 'Full list of icons you can find here: <a href="http://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website#codes" target="_blank">ET-Line Font</a>. You have copy-paste only classname, for example "icon-document".', 'layerswp' ); ?>
											</small>
										</p>
										<p class="layers-form-item<?php echo esc_attr($number_vis); ?>">
											<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $number_name ); ?>"><?php _e( 'Item percent' , 'layerswp' ); ?></label>
											<?php echo $this->form_elements()->input(
												array(
													'type' => 'number',
													'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, $number_name ),
													'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $number_name ),
													'value' => ( isset( $$number_name ) ) ? $$number_name : NULL ,
													'class' => 'layers-number custom_js_number',
													'min' => 1,
													'max' => 100,
													'step' => 1,
												)
											); ?>
										</p>
										<p class="layers-form-item<?php echo esc_attr($excerpt_vis); ?>">
											<label for="<?php echo $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $excerpt_name ); ?>"><?php _e( 'Item description' , 'layerswp' ); ?></label>
											<?php echo $this->form_elements()->input(
												array(
													'type' => 'textarea',
													'name' => $this->get_custom_field_name( $widget_details, 'slides',  $slide_guid, $excerpt_name ),
													'id' => $this->get_custom_field_id( $widget_details, 'slides',  $slide_guid, $excerpt_name ),
													'placeholder' => __( 'Description' , 'layerswp' ),
													'value' => ( isset( $$excerpt_name ) ) ? $$excerpt_name : NULL ,
													'class' => 'layers-form-item layers-textarea custom_js_desc',
													'rows' => 4,
												)
											); ?>
										</p>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
					</section>
				</li>
		<?php }
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Miresume_Slider_Widget");
}