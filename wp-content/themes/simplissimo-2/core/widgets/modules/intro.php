<?php  /**
 * Intro Widget
 *
 * This file is used to register and display the Layers - Intro widget.
 *
 * @package Simplissimo
 * @since Simplissimo 1.0
 */
if( !class_exists( 'Simplissimo_Intro_Widget' ) ) {
	class Simplissimo_Intro_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function Simplissimo_Intro_Widget(){
			global $textdomain;
			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Intro' , 'layerswp' );
			$this->widget_id = 'intro';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array(
					/*'test_checkbox'*/
				);

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => 'Hello',
				'excerpt' => 'This is default intro text',
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			 global $wp_customize;

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			$widget = wp_parse_args( $instance , $instance_defaults );

			// Set the background styling & Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );
			?>

			<!--======= ABOUT ME =========-->
			<section class="about-me container <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>">
				<?php if ( isset( $widget['title'] ) && !empty( $widget['title'] ) ) { ?><h3><?php echo esc_attr( $widget['title'] ); ?></h3><?php } ?>
				<div class="row">
					<div class="col-sm-8">
						<?php if ( isset( $widget['excerpt'] ) && !empty( $widget['excerpt'] ) ) { ?><?php echo wpautop( esc_textarea( $widget['excerpt'] ) ); ?><?php } ?>
					</div>

					<!--======= ABOUT ME IMG =========-->
					<div class="col-sm-4"><?php if ( isset( $widget['feat_image'] ) && !empty( $widget['feat_image'] ) ) ?><img class="img-responsive" src="<?php echo wp_get_attachment_url( $widget['feat_image'] ); ?>" alt="About us"></div>
				</div>
			</section>

		<?php }

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					// 'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
				)
			);

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'design' ),
					'id' => $this->get_field_id( 'design' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'Intro' , 'layerswp' ),
					'icon_class' =>'location'
				) ); ?>

				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom clearfix">
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'excerpt' ) ,
									'id' => $this->get_field_id( 'excerpt' ) ,
									'placeholder' =>  __( 'Short Excerpt' , 'layerswp' ),
									'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
									'class' => 'layers-textarea layers-large',
									'rows' => 10,
								)
							); ?>
						</p>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( 'Featured Image' , 'layerswp' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'image',
									'label' => __( 'Featured Image', 'layerswp' ),
									'button_label' => __( 'Choose Image', 'layerswp' ),
									'name' => $this->get_field_name( 'feat_image' ),
									'id' => $this->get_field_id( 'feat_image' ),
									'class' => 'feat_image',
									'value' => ( isset( $feat_image ) ) ? $feat_image : NULL
								)
							); ?>
						</p>
					</div>
				</section>
			</div>

		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Simplissimo_Intro_Widget");
}