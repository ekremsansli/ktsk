<?php  /**
 * Download Widget
 *
 * This file is used to register and display the Layers - Download widget.
 *
 * @package MiResume
 * @since MiResume 1.0
 */
if( !class_exists( 'Miresume_Download_Widget' ) ) {
	class Miresume_Download_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
	 	function Miresume_Download_Widget(){

			/**
			* Widget variables
			*
		 	* @param  	string    		$widget_title    	Widget title
		 	* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
		 	* @param  	string    		$post_type    		(optional) Post type for use in widget options
		 	* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
		 	* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
		 	*/
		 	global $textdomain;
			$this->widget_title = __( 'Download' , $textdomain );
			$this->widget_id = 'download';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array(
					/*'show_google_map',
					'show_address',
					'show_contact_form'
					'test_checkbox'*/
				);

	 		/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => 'Download Resume',
				'excerpt' => 'Thanks a lot for taking the time out and viewing my work.',
				'resume_link' => '#',
				'download_icon' => 'icon-download',
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			global $wp_customize;

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			$widget = wp_parse_args( $instance , $instance_defaults );

			// Set the background & font styling
			$back_options = array();
			if( !empty( $widget[ 'background_image' ] ) ) $back_options['image'] = $widget[ 'background_image' ];
			if( !empty( $widget[ 'background_color' ] ) ) $back_options['color'] = $widget[ 'background_color' ];

			if( !empty( $back_options ) ) layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $back_options ) );

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );
			?>

			<!-- download block start -->
			<div id="<?php echo esc_attr($widget_id); ?>" class="download_block <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>">
				<div class="container">
					<div class="download-link" style="visibility: visible;">
						<a href="<?php echo esc_url( $widget['resume_link'] ); ?>">
							<span class="down-icon"><i class="<?php echo esc_attr( $widget['download_icon']); ?>"></i></span>
							<h2 class="text"><?php echo esc_html( $widget['title'] ); ?></h2></br></br>
						</a>
							<div class="resume_desc"><?php echo wpautop( esc_html( $widget['excerpt'] ), true); ?></div>
					</div>
					<?php $copy = cs_get_option('footer-copy-text');
					if ( isset( $copy ) && !empty( $copy ) ) { ?>
					<div class="copyright" style="visibility: visible;"><?php echo wpautop( $copy, true ); ?></div>
					<?php } ?>
				</div>
			</div>
			<!-- download block end -->

		<?php }

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
					'background' => array(
						'icon-css' => 'icon-photo',
						'label' => __( 'Background', 'layerswp' ),
						'elements' => array(
							'background_image' => array(
								'type' => 'image',
								'label' => __( 'Background Image', 'layerswp' ),
								'button_label' => __( 'Choose Image', 'layerswp' ),
								'name' => $this->get_field_name( 'background_image' ),
								'id' => $this->get_field_id( 'background_image' ),
								'value' => ( isset( $background_image ) ) ? $background_image : NULL
							),
							'background_color' => array(
								'type' => 'color',
								'label' => __( 'Color', 'layerswp' ),
								'name' => $this->get_field_name( 'background_color' ),
								'id' => $this->get_field_id( 'background_color' ),
								'value' => ( isset( $background_color ) ) ? $background_color : NULL
							),
						)
					),
				)
			);

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'design' ),
					'id' => $this->get_field_id( 'design' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'Intro' , 'layerswp' ),
					'icon_class' =>'location'
				) ); ?>

				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom clearfix">
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>
						<div class="layers-row" style="margin-bottom: 20px;">
							<p class="layers-form-item layers-column layers-span-6">
								<label for="<?php echo $this->get_field_id( 'download_icon' ); ?>"><?php _e( 'Download Icon' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'text',
										'name' => $this->get_field_name( 'download_icon' ),
										'id' => $this->get_field_id( 'download_icon' ),
										'placeholder' => __( 'icon-download' , 'layerswp' ),
										'value' => ( isset( $download_icon ) ) ? trim($download_icon, '.') : NULL ,
										'class' => 'layers-text',
									)
								); ?>
								<small class="layers-small-note">
									<?php _e( 'Full list of icons you can find here: <a href="http://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website#codes" target="_blank">ET-Line Font</a>. You have copy-paste only classname, for example "icon-document".', 'layerswp' ); ?>
								</small>
							</p>
							<p class="layers-form-item layers-column layers-span-6">
								<label for="<?php echo $this->get_field_id( 'resume_link' ); ?>"><?php _e( 'Resume Link' , 'layerswp' ); ?></label>
								<?php echo $this->form_elements()->input(
									array(
										'type' => 'text',
										'name' => $this->get_field_name( 'resume_link' ),
										'id' => $this->get_field_id( 'resume_link' ),
										'placeholder' => __( 'http://' , 'layerswp' ),
										'value' => ( isset( $resume_link ) ) ? $resume_link : NULL ,
										'class' => 'layers-text',
									)
								); ?>
							</p>
						</div>
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'excerpt' ) ,
									'id' => $this->get_field_id( 'excerpt' ) ,
									'placeholder' =>  __( 'Short Excerpt' , 'layerswp' ),
									'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
									'class' => 'layers-textarea layers-large',
								)
							); ?>
						</p>
					</div>

				</section>
			</div>

		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Miresume_Download_Widget");
}