<?php  /**
 * Team Widget
 *
 * This file is used to register and display the Layers - Team widget.
 *
 * @package Simplissimo
 * @since Simplissimo 1.0
 */
if( !class_exists( 'Simplissimo_Team_Widget' ) ) {
	class Simplissimo_Team_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function Simplissimo_Team_Widget(){
			global $textdomain;
			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Team' , 'layerswp' );
			$this->widget_id = 'team';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array(
					/*'test_checkbox'*/
				);

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => 'Our team',
				'excerpt' => 'This is default team section text text',
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			 global $wp_customize;

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			$widget = wp_parse_args( $instance , $instance_defaults );

			// Set the background styling & Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );
			?>

			<section class="about-me container <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>">
				<?php if ( isset( $widget['title'] ) && !empty( $widget['title'] ) ) { ?><h3><?php echo esc_attr( $widget['title'] ); ?></h3><?php } ?>
				<div class="row">
					<div class="col-md-12">
						<?php if ( isset( $widget['excerpt'] ) && !empty( $widget['excerpt'] ) ) { ?><?php echo wpautop( esc_textarea( $widget['excerpt'] ) ); ?><?php } ?>
					</div>
				</div>
				<div class="row">
					<div class="out-team">
						<?php
							$team = cs_get_option( 'team' );
							$clients = cs_get_option( 'clients' );

							if ( !empty( $team ) ) {
								foreach ($team as $staff) { ?>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="team-block">
											<?php if ( isset( $staff['staff-photo'] ) && !empty( $staff['staff-photo'] ) ) { ?><img src="<?php echo esc_url( $staff['staff-photo'] ); ?>" alt="<?php echo esc_textarea( $staff['staff-name'] ); ?>"><?php } ?>
											<?php if ( isset( $staff['staff-name'] ) && !empty( $staff['staff-name'] ) ) { ?><h4><?php echo esc_textarea( $staff['staff-name'] ); ?></h4><?php } ?>
											<?php if ( isset( $staff['staff-profession'] ) && !empty( $staff['staff-profession'] ) ) { ?><div class="jobs"><span><?php echo esc_textarea( $staff['staff-profession'] ); ?></span></div><?php } ?>
											<?php if ( isset( $staff['staff-desc'] ) && !empty( $staff['staff-desc'] ) ) { ?><span><?php echo esc_textarea( $staff['staff-desc'] ); ?></span><?php } ?>
											<ul class="social_icons">
												<?php if ( isset( $staff['staff-facebook'] ) && !empty( $staff['staff-facebook'] ) ) { ?><li class="facebook"><a href="<?php echo esc_url( $staff['staff-facebook'] ); ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
												<?php if ( isset( $staff['staff-twitter'] ) && !empty( $staff['staff-twitter'] ) ) { ?><li class="twitter"><a href="<?php echo esc_url( $staff['staff-twitter'] ); ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
												<?php if ( isset( $staff['staff-dribbble'] ) && !empty( $staff['staff-dribbble'] ) ) { ?><li class="dribbble"><a href="<?php echo esc_url( $staff['staff-dribbble'] ); ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
												<?php if ( isset( $staff['staff-linkedin'] ) && !empty( $staff['staff-linkedin'] ) ) { ?><li class="linkedin"><a href="<?php echo esc_url( $staff['staff-linkedin'] ); ?>"><i class="fa fa-linkedin"></i></a></li><?php } ?>
											</ul>
										</div>
									</div>
						<?php	}
							}
						?>
					</div>
				</div>
				<div class="row">
					<div class="logotype">
						<?php if ( !empty( $clients ) ) {
								foreach ($clients as $client) {
									if ( isset( $client['client-image'] ) && !empty( $client['client-image'] ) ) { ?>
						<div class="logo-wrap col-lg-2 col-md-4 col-sm-4 col-xs-6">
							<img src="<?php echo esc_url( $client['client-image'] ); ?>" alt="<?php echo esc_attr( $client['client-name'] ); ?>" title="<?php echo esc_attr( $client['client-name'] ); ?>">
						</div>
						<?php 	}
							}
						} ?>
					</div>
				</div>
			</section>

		<?php }

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					// 'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
				)
			);

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'design' ),
					'id' => $this->get_field_id( 'design' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'Team' , 'layerswp' ),
					'icon_class' =>'location'
				) ); ?>

				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom clearfix">
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'excerpt' ) ,
									'id' => $this->get_field_id( 'excerpt' ) ,
									'placeholder' =>  __( 'Short Excerpt' , 'layerswp' ),
									'value' => ( isset( $excerpt ) ) ? $excerpt : NULL ,
									'class' => 'layers-textarea layers-large',
									'rows' => 10,
								)
							); ?>
						</p>
					</div>
				</section>
			</div>

		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Simplissimo_Team_Widget");
}