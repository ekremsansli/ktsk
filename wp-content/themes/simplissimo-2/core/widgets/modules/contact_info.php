<?php  /**
 * Contact Info Widget
 *
 * This file is used to register and display the Layers - Contact Info widget.
 *
 * @package Simplissimo
 * @since Simplissimo 1.0
 */
if( !class_exists( 'Simplissimo_Contact_Info_Widget' ) ) {
	class Simplissimo_Contact_Info_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function Simplissimo_Contact_Info_Widget(){
			global $textdomain;
			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Contact Info' , 'layerswp' );
			$this->widget_id = 'contact_info';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array(
					/*'test_checkbox'*/
				);

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'obox-layers-' . $this->widget_id .'-widget', 'description' => __( 'This widget is used to display your ', $textdomain) . $this->widget_title . '.' );

			/* Widget control settings. */
			$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL, 'height' => NULL, 'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id );

			/* Create the widget. */
			parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id , $this->widget_title, $widget_ops, $control_ops );

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => 'Address',
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			 global $wp_customize;

			// Turn $args array into variables.
			extract( $args );

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			$widget = wp_parse_args( $instance , $instance_defaults );

			// Set the background styling & Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $widget );
			?>

			<?php
			$address = cs_get_option('address');
			$phones = cs_get_option('phones');
			$emails = cs_get_option('emails');
			?>

			<section class="post-sec contact-me container <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>"><section class="address-wrapp">
			<?php if ( isset( $widget['title'] ) && !empty( $widget['title'] ) ) { ?><h4><?php echo esc_attr( $widget['title'] ); ?></h4><?php } ?>
				<div class="row">
					<div class="col-md-4">
						<div class="adress-block">
							<h3>Address</h3>
							<ul>
								<li><?php if ( !empty( $address ) ) { print str_replace( "\n", '</li><li>', $address ); } ?></li>
							</ul>
						</div>
					</div>
					<?php if ( !empty( $emails ) ) {
						print '<div class="col-md-4"><div class="adress-block"><h3>E-mail:</h3>';
						foreach ($emails as $email) {
							print '<a target="_blank" href="mailto:' . $email['email'] . '">' . $email['email'] . '</a><br/>';
						}
						print '</div></div>';
					}
					if ( !empty( $phones ) ) {
						print '<div class="col-md-4"><div class="adress-block"><h3>Phone</h3>';
						foreach ($phones as $phone) {
							print '<a target="_blank" href="callto:' . preg_replace('/\D+/', '', $phone['number']) . '">' . $phone['number'] . '</a><br/>';
						}
						print '</div></div>';
					} ?>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="map">
							<?php if ( isset( $widget['iframe'] ) && !empty( $widget['iframe'] ) ) { ?><?php print $widget['iframe']; ?><?php } ?>
						</div>
					</div>
				</div>
			</section>
		</section>

		<?php }

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// $instance Defaults
			$instance_defaults = $this->defaults;

			// If we have information in this widget, then ignore the defaults
			if( !empty( $instance ) ) $instance_defaults = array();

			// Parse $instance
			$instance = wp_parse_args( $instance, $instance_defaults );
			extract( $instance, EXTR_SKIP );

			$design_bar_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_components' , array(
					// 'custom',
					'advanced'
				) );

			$design_bar_custom_components = apply_filters( 'layers_' . $this->widget_id . '_widget_design_bar_custom_components' , array(
				)
			);

			$this->design_bar(
				'side', // CSS Class Name
				array(
					'name' => $this->get_field_name( 'design' ),
					'id' => $this->get_field_id( 'design' ),
				), // Widget Object
				$instance, // Widget Values
				$design_bar_components, // Standard Components
				$design_bar_custom_components // Add-on Components
			); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'Intro' , 'layerswp' ),
					'icon_class' =>'location'
				) ); ?>

				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom clearfix">
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_field_name( 'title' ) ,
									'id' => $this->get_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'layerswp' ),
									'value' => ( isset( $title ) ) ? $title : NULL ,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</p>
						<p class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'textarea',
									'name' => $this->get_field_name( 'iframe' ) ,
									'id' => $this->get_field_id( 'iframe' ) ,
									'placeholder' =>  __( 'Map iframe' , 'layerswp' ),
									'value' => ( isset( $iframe ) ) ? $iframe : NULL ,
									'class' => 'layers-textarea layers-large',
									'rows' => 10,
								)
							); ?>
						</p>
					</div>
				</section>
			</div>

		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Simplissimo_Contact_Info_Widget");
}