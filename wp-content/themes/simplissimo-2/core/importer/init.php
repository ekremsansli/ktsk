<?php
/**
 * Version 0.0.2
 */

require dirname( __FILE__ ) .'/importer/simplissimo-importer.php'; //load admin theme data importer

class SIMPLISSIMO_Theme_Demo_Data_Importer extends SIMPLISSIMO_Theme_Importer {

    /**
     * Holds a copy of the object for easy reference.
     *
     * @since 0.0.1
     *
     * @var object
     */
    private static $instance;
     
    /**
     * Set the key to be used to store theme options
     *
     * @since 0.0.2
     *
     * @var object
     */
    // public $theme_option_name = 'redux_options.json'; //set theme options name here
	
	// public $content_demo_file_name  =  'sample-data-portfolio.xml';

    public $widget_demo_file_name  =  'widgets.wie';

    
	/**
	 * Holds a copy of the widget settings 
	 *
	 * @since 0.0.2
	 *
	 * @var object
	 */
	public $widget_import_results;
	
    /**
     * Constructor. Hooks all interactions to initialize the class.
     *
     * @since 0.0.1
     */
    public function __construct() {
    
		$this->demo_files_path = dirname(__FILE__) . '/demo-files/';

        self::$instance = $this;
		parent::__construct();

    }

	/**
	 * Add menus
	 *
	 * @since 0.0.1
	 */
	// public function set_demo_menus(){

 //        // Menus to Import and assign - you can remove or add as many as you want
 //        $main_menu   = get_term_by('name', 'Main Navigation', 'primary-menu');
 //        $sec_menu    = get_term_by('name', 'Secondary Navigation', 'secondary-menu');

 //        set_theme_mod( 'nav_menu_locations', array(
 //                'primary-menu'       => 'home_top_menu',
 //                'secondary-menu'     => 'top_menu',
 //            )
 //        );

 //    }

}

new SIMPLISSIMO_Theme_Demo_Data_Importer;