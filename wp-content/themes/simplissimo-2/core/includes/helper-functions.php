<?php
/**
 * The template for theme helper functions.
 *
 * @package simplissimo
 * @since 1.0
 */



/**
 * Unregister some parent theme menus
 * @return void
 */
if ( ! function_exists( 'simplissimo_correct_menus' ) ) {
	function simplissimo_correct_menus() {
		unregister_nav_menu( LAYERS_THEME_SLUG . '-secondary-left');
		unregister_nav_menu( LAYERS_THEME_SLUG . '-secondary-right');
		unregister_nav_menu( LAYERS_THEME_SLUG . '-primary-right');
		unregister_nav_menu( LAYERS_THEME_SLUG . '-footer');
		unregister_nav_menu( LAYERS_THEME_SLUG . '-primary');
		register_nav_menu( 'simplissimo-primary', 'Header menu');
	}
	add_action('init', 'simplissimo_correct_menus');
}


/**
 * Unregister unnecessary sidebars
 * @return void
 */
if ( !function_exists( 'simplissimo_unregister_sidebars' ) ) {
	function simplissimo_unregister_sidebars() {
		unregister_sidebar( LAYERS_THEME_SLUG . '-off-canvas-sidebar' );
		unregister_sidebar( LAYERS_THEME_SLUG . '-left-sidebar' );
		for( $footer = 1; $footer < 5; $footer++ ) {
			unregister_sidebar( LAYERS_THEME_SLUG . '-footer-' . $footer);
		} // for footers
		unregister_sidebar( LAYERS_THEME_SLUG . '-left-sidebar' );

		if( class_exists( 'WooCommerce' ) ) {
			unregister_sidebar( LAYERS_THEME_SLUG . '-left-woocommerce-sidebar' );
			unregister_sidebar( LAYERS_THEME_SLUG . '-right-woocommerce-sidebar' );
		}
	}
	add_action( 'widgets_init' , 'simplissimo_unregister_sidebars' , 51 );
}


/**
 * Add needed post formats
 */
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'video', 'quote', 'image', 'audio', 'gallery-slider' ) );



/**
 *
 * Add CodeStar menu item to AdminBar
 *
 * @since 1.0
 * @version 1.0
 *
 */
if ( ! function_exists( 'simplissimo_add_toolbar_items' ) ) {
	function simplissimo_add_toolbar_items( $admin_bar ) {
		global $textdomain;
		$admin_bar->add_menu( array(
			'id'    => 'cs-framework',
			'title' => 'Theme Options',
			'href'  => get_admin_url( 0, 'admin.php?page=cs-framework' ),
			'meta'  => array(
				'title' => __('Theme Options', $textdomain),
				),
			)
		);
	}
	add_action('admin_bar_menu', 'simplissimo_add_toolbar_items', 100);
}



/**
 * Add custom styles to dashboard
 * @return void
 */
if ( !function_exists( 'simplissimo_admin_scripts' ) ) {
	function simplissimo_admin_scripts() {

		wp_register_style( 'custom_admin_css', get_stylesheet_directory_uri().'/assets/css/admin.css' );
		wp_enqueue_style( 'custom_admin_css' );

		wp_register_script('admin_custom_js', get_stylesheet_directory_uri() . '/assets/js/admin.js', false, '1.0', true );
		wp_enqueue_script( 'admin_custom_js' );
	}
	add_action('admin_enqueue_scripts', 'simplissimo_admin_scripts');
}



/**
 * Add required info to header start
 * @return void
 */
if ( !function_exists( 'simplissimo_additional_head' ) ) {
	function simplissimo_additional_head() {
		$output = '<meta charset="' . get_bloginfo( 'charset' ) . '">' . "\n";
		$output .= '<meta name="viewport" content="width=device-width, initial-scale=1">' . "\n";
		$output .= '<meta name="keywords" content="HTML5,CSS3,HTML,Template,Multi-Purpose,M_Adnan,Corporate Theme,HTML5 Theme,blog" >' . "\n";
		$output .= '<meta name="description" content="Simplissimo WP">' . "\n";
		$output .= '<meta name="author" content="Multia-Studio">' . "\n";
		$favicon = cs_get_option( 'favicon' );
		if ( $favicon != '' ) {
			$output .= '<link rel="shortcut icon" href="' . $favicon . '" type="image/x-icon">' . "\n";
		}
		print $output;
	}
	add_action( 'wp_head', 'simplissimo_additional_head', 1 );
}



/**
 * Add required scripts for IE to header end
 * @return void
 */
if ( !function_exists( 'simplissimo_additional_head_ie' ) ) {
	function simplissimo_additional_head_ie() {
		$output = '<!--[if lt IE 9]>' . "\n";
		$output .= '	<script src="' . get_stylesheet_directory_uri() . '/assets/js/html5shiv.js"></script>' . "\n";
		$output .= '	<script src="' . get_stylesheet_directory_uri() . '/assets/js/respond.min.js"></script>' . "\n";
		$output .= '<![endif]-->' . "\n";
		print $output;
	}
	add_action( 'wp_head', 'simplissimo_additional_head_ie', 30 );
}



/**
 *
 * @return void
 */
if ( !function_exists( 'simplissimo_help_functions' ) ) {
	function simplissimo_help_functions()
	{
		add_theme_support( 'custom-header', $args );
		add_theme_support( 'custom-background', $args );
		the_post_thumbnail();
	}
}



/**
 * Header menu function
 * @return void
 */
if ( !function_exists( 'simplissimo_top_menu' ) ) {
	function simplissimo_top_menu( $version = 'light' ) {
		$menu_args = array(
			'theme_location'  => 'simplissimo-primary',
			'container'       => 'div',
			'container_class' => 'collapse navbar-collapse',
			'container_id'	  => 'nav-respo',
			'menu_class'	  => 'nav navbar-nav',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'walker'		  => new Simplissimo_Walker_Nav_Menu(),
		);
		if ( has_nav_menu( 'simplissimo-primary' ) ) {
			print wp_nav_menu( $menu_args );
		} else {
			print '<span class="no-menu">Please register Header Menu from <a href="' . esc_url( admin_url('nav-menus.php') ) . '" target="_blank">Appearance &gt; Menus</a>';
		}
	}
}


/**
 * Get the header options
 * @return [array] Array with header options
 */
if ( !function_exists( 'simplissimo_get_header_info' ) ) {
	function simplissimo_get_header_info() {
		$header_info = array();
		$header_info['socials'] = cs_get_option('socials');
		$header_info['preloader'] = cs_get_option('show-preloader');
		$header_info['logo'] = cs_get_option('logo');
		$header_info['logo-light'] = cs_get_option('logo-light');
		$header_info['favicon'] = cs_get_option('favicon');
		return $header_info;
	}
}


/**
 * Grab YouTube video ID from url
 * @param  [string]		$url YouTube video url
 * @return [string]		Video ID in success, [bool] false in failure
 */
if ( !function_exists( 'get_youtube_id_from_url' ) ) {
	function get_youtube_id_from_url( $url ) {
		if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match ) ) {
			return $match[1];
		} else {
			return false;
		}
	}
}



if ( !class_exists( 'Simplissimo_Walker_Nav_Menu' ) ) {
	class Simplissimo_Walker_Nav_Menu extends Walker_Nav_Menu {
		/**
		 * Starts the list before the elements are added.
		 */
		public function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat("\t", $depth);
			$output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
		}

		/**
		 * Ends the list of after the elements are added.
		 */
		public function end_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat("\t", $depth);
			$output .= "$indent</ul>\n";
		}

		/**
		 * Start the element output.
		 */
		public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;

			/**
			 * Filter the CSS class(es) applied to a menu item's list item element.
			 */
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
			$is_has_children = strpos($class_names, "menu-item-has-children");
			if ( $is_has_children ) { $class_names .= ' dropdown'; }
			$is_active = strpos($class_names, "current-menu-item");
			if ( $is_active ) { $class_names .= ' active'; }
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

			/**
			 * Filter the ID applied to a menu item's list item element.
			 */
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $class_names .'>';

			$atts = array();
			$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
			$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
			$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
			$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

			/**
			 * Filter the HTML attributes applied to a menu item's anchor element.
			 */
			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}

			$item_output = $args->before;
			$attributes .= ( $is_active ) ? ' class="active"' : '';
			$item_output .= '<a'. $attributes .'>';
			/** This filter is documented in wp-includes/post-template.php */
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ( $is_has_children ) ? ' <span class="fa fa-angle-down menu-arrow"></span>' : '';
			$item_output .= '</a>';
			$item_output .= $args->after;

			/**
			 * Filter a menu item's starting output.
			 */
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		/**
		 * Ends the element output, if needed.
		 */
		public function end_el( &$output, $item, $depth = 0, $args = array() ) {
			$output .= "</li>\n";
		}

	} // Simplissimo_Walker_Nav_Menu
}


/**
 * [simplissimo_get_instagram_widget description]
 * @return [type] [description]
 */
if ( !function_exists( 'simplissimo_get_instagram_widget' ) ) {
	function simplissimo_get_instagram_widget() {
		$images = cs_get_option( 'footer-inst-gallery' );
		if ( !empty( $images ) ) {
			$images_arr = explode(',', $images);
			$imgs = array();
			foreach ($images_arr as $image) {
				if ( wp_attachment_is_image( $image ) ) {
					$img = wp_get_attachment_image_src( $image, 'full' );
					$imgs[] = $img[0];
				}
			}
		}

		$output = '<div class="row instagram-footer-widget">';
		$output .= '	<div class="col-md-6">';
		$output .= '		<ul class="row">';
		if ( ( isset( $imgs[0] ) ) ) $output .= '			<li class="col-sm-6"> <img class="img-responsive" src="' . $imgs[0] . '" alt=""><img class="img-responsive" src="' . $imgs[1] . '" alt=""> </li>';
		if ( ( isset( $imgs[2] ) ) ) $output .= '			<li class="col-sm-6"> <img class="img-responsive" src="' . $imgs[2] . '" alt=""> </li>';
		$output .= '		</ul>';
		$output .= '	</div>';
		$output .= '	<div class="col-md-6">';
		$output .= '		<ul class="row">';
		$output .= '			<li class="col-sm-3 inst"><a href="' . esc_url( cs_get_option( 'footer-instagram' ) ) . '" target="_blank"><img src="' . get_stylesheet_directory_uri() . '/assets/images/instgram.png" alt=""></a></li>';
		if ( ( isset( $imgs[3] ) ) ) $output .= '			<li class="col-sm-3"><img class="img-responsive" src="' . $imgs[3] . '" alt=""></li>';
		if ( ( isset( $imgs[5] ) ) ) $output .= '			<li class="col-sm-6"><img class="img-responsive" src="' . $imgs[5] . '" alt=""></li>';
		if ( ( isset( $imgs[4] ) ) ) $output .= '			<li class="col-sm-3"><img class="img-responsive" src="' . $imgs[4] . '" alt=""></li>';
		if ( ( isset( $imgs[6] ) ) ) $output .= '			<li class="col-sm-6"><img class="img-responsive" src="' . $imgs[6] . '" alt=""></li>';
		if ( ( isset( $imgs[7] ) ) ) $output .= '			<li class="col-sm-3"><img class="img-responsive" src="' . $imgs[7] . '" alt=""></li>';
		$output .= '		</ul>';
		$output .= '	</div>';
		$output .= '</div>';
		print $output;
	}
}



/**
 * Get related posts to $post_id
 * @param  post ID
 * @return array in success, false in failure
 */
if ( ! function_exists( 'simplissimo_get_related_posts' ) ) {
	function simplissimo_get_related_posts( $post_id = NULL, $posts_qty = 3 ) {
		global $post;
		$post_id = ( $post_id != NULL )? $post_id : $post->ID;
		$related = get_posts( array( 'category__in' => wp_get_post_categories($post_id), 'orderby' => 'rand', 'numberposts' => $posts_qty, 'post__not_in' => array($post_id) ) );
		return ( $related ) ? $related : false;
	}
}



/**
 * [add_user_social_links description]
 * @param [type] $contactmethods [description]
 */
if ( ! function_exists('simplissimo_add_user_social_links') ) {
	function simplissimo_add_user_social_links( $contactmethods ) {
		$contactmethods['twitter'] 		= 'Twitter';
		$contactmethods['facebook'] 	= 'Facebook';
		$contactmethods['vimeo'] 		= 'Vimeo';
		$contactmethods['google'] 		= 'Google+';
		$contactmethods['dribbble'] 	= 'Dribbble';

		return $contactmethods;
	}
	add_filter('user_contactmethods','simplissimo_add_user_social_links',10,1);
}



/**
 * [simplissimo_get_author_info description]
 * @param  [type] $user_id [description]
 * @return [type]          [description]
 */
if ( !function_exists( 'simplissimo_get_author_info' ) ) {
	function simplissimo_get_author_info( $user_id ) {
		$author = array();
		$author['name'] = get_the_author_meta( 'user_nicename', $user_id );
		$author['desc'] = get_the_author_meta( 'description', $user_id );
		$author['facebook'] = get_the_author_meta( 'facebook', $user_id );
		$author['twitter'] = get_the_author_meta( 'twitter', $user_id );
		$author['google'] = get_the_author_meta( 'google', $user_id );
		$author['vimeo'] = get_the_author_meta( 'vimeo', $user_id );
		$author['dribbble'] = get_the_author_meta( 'dribbble', $user_id );
		$author['email'] = get_the_author_meta( 'user_email', $user_id );
		$author['avatar'] = get_avatar( $user_id, 96 );;

		return $author;
	}
}



/**
* Fake Apply Inline Styles
*/
if( !function_exists( 'layers_apply_inline_styles' ) ) {
	function layers_apply_inline_styles(){
		return false;
	}
} // layers_apply_inline_styles



/**
* Fake Apply Custom CSS
*/
if( !function_exists( 'layers_apply_custom_styles' ) ) {
	function layers_apply_custom_styles(){
		return false;
	}
} // layers_apply_custom_styles



