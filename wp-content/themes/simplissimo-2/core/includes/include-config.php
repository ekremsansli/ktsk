<?php
/**
 * Loads Shortcodes and other Stuffs.
 *
 * @package simplissimo
 * @since 1.0
 */



// Import Integration
// ----------------------------------------------------------------------------------------------------
// locate_template('core/importer/init.php', true);



// TGM Integration
// ----------------------------------------------------------------------------------------------------
locate_template('core/plugins/tgm/class-tgm-plugin-activation.php', true);



// AQ_resizer include
// ----------------------------------------------------------------------------------------------------
locate_template('core/plugins/aq_resizer.php', true);