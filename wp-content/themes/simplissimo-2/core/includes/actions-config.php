<?php
/**
 * The template for requried actions hooks.
 *
 * @package simplissimo
 * @since 1.0
 */



/*-------------------------------------------------------------------------------------------------------------
CodeStar Integration
-------------------------------------------------------------------------------------------------------------*/
require_once get_stylesheet_directory() . '/core/cs-framework/cs-framework.php';



/**
 * Include Simplissimo custom widgets (based on LayersWP theme)
 */
require_once get_stylesheet_directory() . '/core/widgets/init.php';

/**
* @return null
* @param none
* @loads all the js and css script to frontend
**/
if ( !function_exists( 'simplissimo_enqueue_scripts' ) ) {
function simplissimo_enqueue_scripts() {

	// register styles
	wp_enqueue_style( 'bootstrap',			get_stylesheet_directory_uri() . '/assets/css/bootstrap.css' );
	wp_enqueue_style( 'plugins',			get_stylesheet_directory_uri() . '/assets/css/plugins.css' );
	wp_enqueue_style( 'style',				get_stylesheet_directory_uri() . '/assets/css/style.css' );
	wp_enqueue_style( 'responsive',			get_stylesheet_directory_uri() . '/assets/css/responsive.css' );
	wp_enqueue_style( 'fancybox',			get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.css' );
	//register scripts
	wp_enqueue_script( 'bootstrap',					get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js',				array(), false, true );
	wp_enqueue_script( 'jquery-flexslider',			get_stylesheet_directory_uri() . '/assets/js/jquery.flexslider-min.js',	 	array(), false, true );
	wp_enqueue_script( 'jquery-fancybox',			get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.js',	 	array(), false, true );
	wp_enqueue_script( 'jquery-magnific-popup',		get_stylesheet_directory_uri() . '/assets/js/jquery.magnific-popup.min.js',	array(), false, true );
	wp_enqueue_script( 'jquery-accordionSlider',	get_stylesheet_directory_uri() . '/assets/js/jquery.accordionSlider.js',	array(), false, true );
	wp_enqueue_script( 'owl-carousel',				get_stylesheet_directory_uri() . '/assets/js/owl.carousel.min.js',	 		array(), false, true );
	wp_enqueue_script( 'isotope',					get_stylesheet_directory_uri() . '/assets/js/isotope.pkgd.min.js',	 		array(), false, true );
	wp_enqueue_script( 'google-maps',				"https://maps.googleapis.com/maps/api/js?v=3.exp",	 						array(), false, true );
	wp_enqueue_script( 'uisearch',					get_stylesheet_directory_uri() . '/assets/js/uisearch.js',					array(), false, true );
	wp_enqueue_script( 'main',						get_stylesheet_directory_uri() . '/assets/js/main.js',	 					array(), false, true );
	wp_dequeue_style( LAYERS_THEME_SLUG . '-framework' );

}
// Register datepicker ui for properties
function admin_homes_for_sale_javascript(){
    global $post;
    if($post->post_type == 'homes-for-sale' && is_admin()) {
        
    }
}
add_action('admin_print_scripts', 'admin_homes_for_sale_javascript');

add_action('admin_print_styles', 'admin_homes_for_sale_styles');

add_action( 'wp_enqueue_scripts', 'simplissimo_enqueue_scripts', 20);
}