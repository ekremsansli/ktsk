		</div><?php /* Content end */ ?>

		<!--
		<footer>
			<?php simplissimo_get_instagram_widget(); ?>
			<div class="container">
				<div class="rights">
					<?php echo wpautop( esc_textarea( cs_get_option( 'footer-copy-text' ) ) ); ?>

					<a id="back-to-top" class="back-top" href="#wrap">Top</a>

				</div>
			</div>
		</footer>
	</div><?php /* Wrap end */ ?>
	<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="pop_up">
			<h3>Be in the Know.</h3>
			<p>Be the first to subscribe to our newsletter and <br>
			receive our latest news!</p>
			<div class="form-group">
				<input type="text" class="form-control" id="name" placeholder="First Name">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" id="email" placeholder="Valid Email">
			</div>
			<button class="btn"> Subscribe</button>
			<span>Your information will never be shared or sold to a 3rd party and you can unsubscribe at any time.</span>
		</div>
	</div>
	-->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="left col-md-6 col-sm-6 col-xs-12">
						<p class="text1">
							KTSK’nın misyonu Koç Topluluğu Şirketleri çalışanlarından başlayarak toplumun her kesiminde spor faaliyetlerinin yaygınlaştırılmasına destek vermektedir.
						</p>
						<img src="/wp-content/themes/simplissimo-2/assets/images/koc-logo.png" />
					</div>
					<div class="right col-md-6 col-sm-6 col-xs-12">
						<div class="menu-header col-md-12 col-sm-12 col-xs-12">
							MENÜ
						</div>
						<div class="l1 col-md-6 col-sm-6 col-xs-12 footer-menu">
							<ul class="">
								<li><a href="/">ANASAYFA</a></li>
								<li><a href="/hakkimizda-2/">BİZ KİMİZ</a></li>
								<li><a href="/cocuklar-icin/">ÇOCUKLAR İÇİN</a></li>
								<li><a href="/eglence-icin/">EĞLENCE İÇİN</a></li>
							</ul>
						</div>
						<div class="l2 col-md-6 col-sm-6 col-xs-12 footer-menu">
							<ul class="">
								<li><a href="/kupa-icin/">KUPA İÇİN</a></li>
								<li><a href="/yarisma-icin-fikstur/">YARIŞMA İÇİN</a></li>
								<li><a href="/iletisim/">İLETİŞİM</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="cfooter">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 cf-inner">
					<div class="col-md-12 col-xs-12">
						<p style="text-align: left;float:left;width:80%;">Koç Topluluğu Spor Kulübü © 2012</p>
						<a href="http://rhinorunner.com/" target="_blank"><img style="width:40px!important;height:40px!important;float: right;margin-top: 6px;" src="/wp-content/themes/simplissimo-2/assets/images/rhino-footer.png" /></a>

					</div>

					
					
				</div>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-71194628-1', 'auto');
  ga('send', 'pageview');
 
</script>
</html>