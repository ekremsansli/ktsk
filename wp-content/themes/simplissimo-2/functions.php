<?php

global $textdomain;
$textdomain = 'simplissimo';
function kucult($kelime){
$kucuk=array('i','ı','ğ','ö','ü','ş','ç');
$buyuk=array('İ','I','Ğ','Ö','Ü','Ş','Ç');
$kelime=str_replace($buyuk,$kucuk,$kelime);
$kelime=ucwords(strtolower($kelime));

return $kelime ;
}
function bartag_func( $atts ) {
  return '<a href="/kurs-kayit/?kurs_id='.$atts['calendar_id'].'&kurs_isim='.$atts['name'].'"><div class="hbtn"><p style="color:white">KURS KAYIT</p></div></a>';
}
add_shortcode( 'kurs_kayit', 'bartag_func' );
function get_excerpt($count,$id,$content){
    $permalink = get_permalink(id);
    $excerpt = $content;
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt."...";
    return $excerpt;
}
function slugify($text)
{ 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}
function zamantr($girdi) 
{ 
    $cikti = $girdi; 
$aylarIng = array( 
    "January", "February", "March", "April", "May", "June",  
    "July", "August", "September", "October", "November", "December" 
    ); 
$gunlerIng = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"); 
$aylar = array( 
    "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran",  
    "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" 
    ); 
$gunler = array("Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi", "Pazar"); 
$cikti = str_replace($aylarIng, $aylar, $cikti); 
$cikti = str_replace($gunlerIng, $gunler, $cikti); 
return $cikti; 
} 

add_action( 'wp_ajax_nopriv_load-filter', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_load-filter', 'prefix_load_cat_posts' );

add_action( 'wp_ajax_nopriv_load-filter2', 'prefix_load_cat_posts2' );
add_action( 'wp_ajax_load-filter2', 'prefix_load_cat_posts2' );

add_action( 'wp_ajax_nopriv_load-filtere', 'prefix_load_cat_posts3' );
add_action( 'wp_ajax_load-filtere', 'prefix_load_cat_posts3' );


add_action( 'wp_ajax_nopriv_load-filtercat', 'prefix_load_cat_posts4' );
add_action( 'wp_ajax_load-filtercat', 'prefix_load_cat_posts4' );

add_action( 'wp_ajax_nopriv_load-filterhaber', 'prefix_load_cat_posts5' );
add_action( 'wp_ajax_load-filterhaber', 'prefix_load_cat_posts5' );

add_action( 'wp_ajax_nopriv_load-filtersonuc', 'prefix_load_cat_posts6' );
add_action( 'wp_ajax_load-filtersonuc', 'prefix_load_cat_posts6' );

add_action( 'wp_ajax_nopriv_load-filterpuan', 'prefix_load_cat_posts7' );
add_action( 'wp_ajax_load-filterpuan', 'prefix_load_cat_posts7' );

add_action( 'wp_ajax_nopriv_load-filtermail', 'prefix_load_cat_mail' );
add_action( 'wp_ajax_load-filtermail', 'prefix_load_cat_mail' );

function prefix_load_cat_mail() {
  $to = 'info@ktsk.com.tr';
  $subject = $_POST['subject'];
  $body = "İsim - Soyisim: ".$_POST['name']."<br/>";
  $body .= "Telefon: ".$_POST['phone']."<br/>";
  $body .= "Başlık: ".$_POST['subject']."<br/><br/>";
  $body .= $_POST['message'];
  $headers = array('Content-Type: text/html; charset=UTF-8','From: '.$_POST['name'].' <'.$_POST['from'].'>');
  wp_mail( $to, $subject, $body, $headers );
}
function prefix_load_cat_posts7() {
    $cats = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    foreach ($cats as $key => $value) {
      if($value->name == "Puan Durumu"){
        $posts = get_posts(array('cat'=>$value->cat_ID));
        echo json_encode($posts);
        die(1);
      }
    } 
}

function prefix_load_cat_posts6() {
    $cats = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    foreach ($cats as $key => $value) {
      if($value->name == "Sonuçlar"){
        $posts = get_posts(array('cat'=>$value->cat_ID));
        foreach ($posts as $key => $value) {
            $d = strtotime(get_post_meta($value->ID)['tarih'][0]);
            $posts[$key]->tarih = date('d',$d)."/".date('m',$d)."/".date('Y',$d);
            $posts[$key]->sehir = get_post_meta($value->ID)['Şehir'][0];
            $posts[$key]->brans = get_post_meta($value->ID)['Branş'][0];
            $posts[$key]->sonuc = get_post_meta($value->ID)['Sonuç'][0];
            $posts[$key]->tagss = wp_get_post_tags($value->ID);
            $posts[$key]->permalink = get_permalink($value->ID);
            $posts[$key]->thumbnail = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
          }
        echo json_encode($posts);
       die(1);
      }
    } 
}

function prefix_load_cat_posts5() {
    $cats = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    foreach ($cats as $key => $value) {
      if($value->name == "Haberler"){
        $posts = get_posts(array('cat'=>$value->cat_ID));
        foreach ($posts as $key => $value) {
            $d = strtotime(get_post_meta($value->ID)['tarih'][0]);
            $posts[$key]->tarih = date('d',$d)."/".date('m',$d)."/".date('Y',$d);
            $posts[$key]->sehir = get_post_meta($value->ID)['Şehir'][0];
            $posts[$key]->brans = get_post_meta($value->ID)['Branş'][0];
            $posts[$key]->sonuc = get_post_meta($value->ID)['Sonuç'][0];
            $posts[$key]->tagss = wp_get_post_tags($value->ID);
            $posts[$key]->permalink = get_permalink($value->ID);
            $posts[$key]->thumbnail = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
          }
        echo json_encode($posts);
       die(1);
      }
    } 
}

function prefix_load_cat_posts4() {
    $cats = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    foreach ($cats as $key => $value) {
      if($value->name == "Fikstür"){
        $posts = get_posts(array('cat'=>$cats[0]->cat_ID));
        echo json_encode($posts);
        die(1);
      }
    } 
}

function prefix_load_cat_posts3() {
    $cats = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    $cat_output = array();
    foreach ($cats as $key => $cat) {
      if(strpos($cat->name,'Lig')){
        $cat_output[] = $cat;
      }
    }
    echo json_encode($cat_output);
   die(1);
}

function get_top_parent($cat){
  $curr_cat = get_category_parents($cat, false, '/' ,true);
  $curr_cat = explode('/',$curr_cat);
  $idObj = get_category_by_slug($curr_cat[1]);
  return $idObj->name;
}
function prefix_load_cat_posts () {
    $categories = get_categories('child_of='.$_POST[ 'cat' ].'&hide_empty=0');
    foreach ($categories as $key => $value) {
      $categories[$key]->brans = get_top_parent($value->term_id);
    }
   echo json_encode($categories);
   die(1);
}
function prefix_load_cat_posts2 () {
  $posts = get_posts(array('cat'=>$_POST['cat']));

      foreach ($posts as $key => $value) {
        $d = strtotime(get_post_meta($value->ID)['tarih'][0]);
        $posts[$key]->tarih = date('d',$d)."/".date('m',$d)."/".date('Y',$d);
        $posts[$key]->sehir = get_post_meta($value->ID)['Şehir'][0];
        $posts[$key]->brans = get_post_meta($value->ID)['Branş'][0];
        $posts[$key]->sonuc = get_post_meta($value->ID)['Sonuç'][0];
        $posts[$key]->tagss = wp_get_post_tags($value->ID);
        $posts[$key]->permalink = get_permalink($value->ID);
        $posts[$key]->thumbnail = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
      }
    
   echo json_encode($posts);
   die(1);
   }
/**
 * Include theme required actions hooks
 */


locate_template	( 'core/includes/include-config.php',	true );
locate_template	( 'core/includes/actions-config.php',	true );
locate_template	( 'core/includes/helper-functions.php',	true );

