<?php
/**
 * The template for displaying a single post
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<section id="post-<?php the_ID(); ?>" <?php if(get_the_category()[0]->term_id != 170)post_class( 'content-main clearfix container' ); ?>>
	<?php if( have_posts() ) : ?>
		<?php
			$col = "column span-8";
			if(get_the_category()[0]->term_id == 170) {
				$col = "column span-12";
			}
		?>
		<?php while( have_posts() ) : the_post();?>
			<div class="<?php echo $col; ?>">
				<article>

					<?php
						if(get_the_category()[0]->term_id == 170) 
							get_template_part( 'partials/content', 'rezerve' ); 
						else
							get_template_part( 'partials/content', 'single' ); 
					?>
				</article>

				<?php
				?>
			</div>
		<?php endwhile; // while has_post(); ?>

	<?php endif; // if has_post() ?>
	<?php 
		if(get_the_category()[0]->term_id != 170){
	?>
	<div class="column span-4 right-bar">
		<?php get_sidebar('post'); ?>
	</div>
	<?php }?>
</section>

<?php get_footer(); ?>
