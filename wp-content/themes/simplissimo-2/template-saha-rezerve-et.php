<?php
/**
 * Template Name: Saha Rezerve Et
 * The template for displaying imagewall blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			<div class="col-md-12">
				<?php
				if ( is_front_page() ) {
					$paged = (get_query_var('page')) ? get_query_var('page') : 1;
				}
				$args = array(
					"post_type" => "post",
					"paged" => $paged,
					'cat' => "170",
				);

				$wp_query = new WP_Query($args);

				if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
						global $post; ?>
						<?php get_template_part( 'partials/content' , 'gridlist' ); ?>
					<?php endwhile; // while has_post(); ?>

					<?php the_posts_pagination(); ?>
				<?php endif; // if has_post() ?>
			</div>
		</div>
	</section>
</div>
<?php get_footer();