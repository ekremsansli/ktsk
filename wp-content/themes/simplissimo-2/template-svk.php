<?php
/**
 * Template Name: Spor Okulu ve Kurslar
 * The template for displaying imagewall blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			
			<div class="col-md-8">
				<div class="col-md-12" style="margin-bottom:15px;padding:0!important;">

					<div id="slider-svk">
						<ul class="slides">
							<?php 
						$args = array(
							"post_type" => "post",
							"paged" => $paged,
							'category_name' => "Spor Okulu ve Kurslar Slider",
						);
						$wp_query = new WP_Query($args);
						if( $wp_query->have_posts() ) : ?>
							<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
								global $post; ?>
								<?php echo "
								<li>
									<img width='785' height='492' src='".wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),array(785,492))[0]."'/>
									<div class='flex-caption'>".get_the_title($post->ID)."</div>
								</li>"; ?>
							<?php endwhile; // while has_post(); ?>
						<?php endif; // if has_post() 
					?>
						</ul>
					</div>
				</div>
				<?php
				if ( is_front_page() ) {
					$paged = (get_query_var('page')) ? get_query_var('page') : 1;
				}
				$args = array(
					"post_type" => "post",
					"paged" => $paged,
					'cat' => '48,49',
				);

				$wp_query = new WP_Query($args);

				if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
						global $post; ?>
						<?php get_template_part( 'partials/content' , 'list' ); ?>
					<?php endwhile; // while has_post(); ?>

					<?php the_posts_pagination(); ?>
				<?php endif; // if has_post() ?>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4">
				<div class="right-bar">
					<?php get_sidebar( 'svk' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();