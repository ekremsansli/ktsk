<?php
/**
 * The template for displaying a page
 *
 * @package Layers
 * @since Layers 1.0.0
 */

get_header(); ?>

<section id="post-<?php the_ID(); ?>" <?php post_class( 'content-main clearfix' ); ?>>
	<div class="row">
		<div class="col-md-12"><?php if( have_posts() ) : ?>
				<?php while( have_posts() ) : the_post(); ?>
					<article <?php post_class('single-page'); ?>>
						<h1 class="page_title"><?php the_title(); ?></h1>
						<div class="page_content"><?php the_content(); ?></div>
					</article>
				<?php endwhile; // while has_post(); ?>
			<?php endif; // if has_post() ?>
		</div>
		<!--======= RIGHT SIDE BAR =========-->
	</div>
</section>

<?php get_footer(); ?>