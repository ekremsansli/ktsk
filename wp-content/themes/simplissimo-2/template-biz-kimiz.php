
<?php
/*
Template Name: Biz Kimiz
*/
get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<?php 
				$args = array(
				    'post_type' => 'page',
				    'include' => array(74),
				    'post_status' => 'publish'
				); 
				$img_url = "";
				$pages = get_pages($args); 
				foreach($pages as $page) {
					$id = get_post_thumbnail_id($page->ID);
					$image_attributes = wp_get_attachment_image_src( $id ,'full');
					$img_url = $image_attributes[0];
				}
			?>
			<?php 
				$class = "";
				$style = "";
				$idd = get_the_ID();
				if($idd == 93 || $idd == 81){
					$class = "col-md-8";
					$style="block";
				}
				else{
					$class = "col-md-12";
					$style="none";
				}
			?>
			<!--======= POST SECTION =========-->
			<div class="<?=$class?>" style="background:white;margin-bottom: 30px;padding:0;">
				<div class="col-md-12 bk-image"  style=" <?php if(get_the_ID() == 93){echo 'background: url(/wp-content/themes/simplissimo-2/assets/images/ktsk_ekip.jpg) no-repeat top left;';}else{echo 'background: url('.$img_url.') no-repeat center left;';}?>">
				</div>
				<div class="col-md-2 bk-menu-outer">
					<ul class="bk-menu">
						<li><a href="/hakkimizda-2">Hakkımızda</a></li>
						<li><a href="/ekibimiz">Ekibimiz</a></li>
						<li><a href="javascript:void(0);" id="ss">Sosyal & Spor</a></li>
						<li>
							<ul style="margin-left:10px;font-size:15px;" id="sub-ul">
								<li><a href="/abdulmecid-efendi-kosku">-Abdülmecid Efendi Köşkü</a></li>
								<li><a href="/baglarbasi-korusu-spor-tesisleri">-Bağlarbaşı Korusu Spor Tesisleri</a></li>
								<li><a href="/baglarbasi-korusu-sosyal-tesisleri">-Bağlarbaşı Korusu Sosyal Tesisleri</a></li>
								<li><a href="/istinye-spor-salonu">-İstinye Spor Salonu</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-10 bk-content">
					<?php 
						$page = get_page(get_the_ID());
					?>
					<p class="title"><?=$page->post_title?></p>
					<div class="page_content">
						<p>
							<?php 

								if(get_the_ID() == 93)
								{
									$args = array(
            							'post_type' => 'takim',
            							'post_status' => 'publish',
            							'order' => 'ASC'
        							);
        							$string = '<div class="row">';
        							$query = new WP_Query( $args );

        							if( $query->have_posts() ){
            							
            							while( $query->have_posts() ){	
            								$string .= '<div class="col-md-4 col-sm-4 col-xs-12 person" >';
                							$query->the_post();
                							$thumb  = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

                							$string .='<div class="p-info"><p class="p-title">'.get_the_title().'</p>
                								<p class="p-mail"><img src="/wp-content/themes/simplissimo-2/assets/images/mail.jpg" width="20" height="15" />'.get_post_meta(get_the_ID(),"Email")[0].'</p>
                								</div>';
                							$string .= '</div>';
            							}
            							$string .= '</div>';
        							}
        							wp_reset_query();
        							echo $string;
								}
								else if(get_the_ID() == "81"){
								?>
									<h1>Koç Topluluğu Spor Kulübü Derneği Üye Listesi</h1>
									<div class="col-md-6" style="padding:0;margin-bottom:30px">
										<ul class="h-ul">
											<li>Arçelik A.Ş.</li>
											<li>Arçelik LG Klima Sanayi ve Ticaret A.Ş.</li>
											<li>Ark İnşaat Sanayi ve Tic. A.Ş.</li>
											<li>Aygaz A.Ş.</li>
											<li>Demir Export A.Ş.</li>
										</ul>
									</div>
									<div class="col-md-6" style="padding:0;margin-bottom:30px">
										<ul class="h-ul">
											<li>Düzey Tüketim Malları San.Paz. ve Tic. A.Ş.</li>
											<li>Entek Elektrik Üretimi A.Ş.</li>
											<li>Ford Otomotiv Sanayi A.Ş.</li>
											<li>Koç Fiat Kredi Finansman A.Ş.</li>
											<li>Koç Finansman A.Ş.</li>
										</ul>
									</div>
									<div class="col-md-6" style="padding:0;margin-bottom:30px">
										<ul class="h-ul">
											<li>Koç Holding A.Ş.</li>
											<li>Koçtaş Yapı Marketleri Tic. A.Ş.</li>
											<li>Moment Eğitim Arast. Sağlık Hizm. Tic. A.Ş.</li>
											<li>Otokar Otomotiv ve Savunma San. A.Ş.</li>
											<li>Otokoç Otomotiv Ticaret ve San. A.Ş.</li>
										</ul>
									</div>
									<div class="col-md-6" style="padding:0;margin-bottom:30px">
										<ul class="h-ul">
											<li>Ram Dış Ticaret A.Ş.</li>
											<li>RMK Marine Gemi Yapım Sanayi ve Deniz Tas.Isl. A.Ş.</li>
											<li>Setur Servis Turistik A.Ş.</li>
											<li>Tanı Pazarlama İletişim Hizm. A.Ş.</li>
											<li>Tat Gıda Sanayi A.Ş.</li>
										</ul>
									</div>

									<div class="col-md-6" style="padding:0;margin-bottom:30px">
										<ul class="h-ul">
											<li>Tofaş Türk Otomobil Fabrikası A.Ş.</li>
											<li>Türk Traktör ve Ziraat Makineleri A.Ş.</li>
											<li>Türkkiye Petrol Rafineleri A.Ş.</li>
											<li>Yapı ve Kredi Bankası A.Ş.</li>
											<li>Zer Merkezi Hizmetler ve Tic A.Ş.</li>
										</ul>
									</div>
									<div class="col-md-6"></div>
									<br/>
									<h1></h1>
									<h1 style="clear:left;">Misyonumuz</h1>
									KTSK’nın misyonu Koç Topluluğu Şirketleri çalışanlarından başlayarak toplumun her kesiminde spor faaliyetlerinin yaygınlaştırılmasına destek vermektedir. KTSK, Koç Topluluğu Şirketleri’nin bulunduğu illerde çalışanlarımız ve aileleri için sportif, sosyal ve kültürel organizasyonlar gerçekleştirmektir.
									<h1></h1>
									<h1>Vizyonumuz</h1>
									Koç Topluluğu Spor Kulübü olarak amacımız spor bilincini yayarak beden ve ruh gelişimini sağlamaktır.						<p></p>
									<br/><br/>
								<?php
								}
								else{
									
									$args = array(
            							'post_status' => 'publish',
            							'order' => 'ASC'
        							);
     								$ccat = get_the_title();
									if(get_the_title() == "İstinye Spor Salonu"){
										$args['cat'] = '15';
									}
									else{
										$args['category_name'] = get_the_title();	
									}
										
        							$query = new WP_Query( $args );
        							$in = 0;
        							$count = $query->post_count;
        							if( $query->have_posts() ){
        								while( $query->have_posts() ){	
        									$query->the_post();
        									$attachments_id = array();
        									$attachments = get_posts( array(
            									'post_type' => 'attachment',
            									'posts_per_page' => -1,
            									'post_parent' => get_the_ID(),
            									'exclude'     => get_post_thumbnail_id(get_the_ID())
											));

											foreach ( $attachments as $attachment ) {
												if($attachment->post_content != "no_slider")
													$attachments_id[] = $attachment->ID;
            								}
            								$string .='<div class="slider-bk">';
          										foreach ($attachments_id as $att) {
          											$string .='
          											<div class="item">
          												<a class="fancybox" rel="gallery'.get_the_ID().'" href="'.wp_get_attachment_image_src($att,'full',true)[0].'"> 
          													<img src="'.wp_get_attachment_image_src($att,array(150,150),true)[0].'"/>
          												</a>
          											</div>';
          										}
											$string .='</div>';
											if($count == 1){
												echo $string.get_the_content();
											}
											else{
												echo '<div class="accordion">
													<div class="accordion-section">
														<a class="accordion-section-title" href="#accordion-'.$in.'">'.get_the_title().'<div></div></a>
														<div id="accordion-'.$in.'" class="accordion-section-content">
															'.$string.'
															<p>'.get_the_content().'</p>
														</div>
													</div>
												</div>';
											}
											
											$in++;
											$string = "";
        								}
        							}
								}
								
							?>
						</p>
					</div>
				</div>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4" style="display:<?=$style?>;margin-bottom: 30px;">
				<div class="right-bar">
					<?php get_sidebar( 'bizkimiz' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();