
<?php $col_4 = is_home() ? $col_4 = ' column span-4':''; ?>
<div id="widget-area" class="widget-area right-sidebar<?php echo esc_attr($col_4);?>" role="complementary">
<div class="container">
	<div class="row">
		<div class="col-md-12 guncel-sidebar">
			<div class="header col-md-12">
				<span>İLGİLİ HABERLER</span>
			</div>
			<div class="inner col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
				<?php
    $orig_post = $post;

    global $post;
    $tags = wp_get_post_tags($post->ID);
    $page_ids = array();
    foreach (get_pages() as $value) {
    	$page_ids[] = $value->ID;
    }
    $page_ids[] = $post->ID;
    if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args=array(
    'tag__in' => $tag_ids,
    'post__not_in' => $page_ids,
    'posts_per_page'=>3,
    );
     
    $my_query = new wp_query( $args );
 
    while( $my_query->have_posts() ) {
    $my_query->the_post();
    ?>
    <div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
		<div class="gimg col-sm-6"><?=get_the_post_thumbnail($post->ID,array(100,100));?></div>
		<div class="gcontent col-sm-6">
			<span class="gtitle"><a href="<?=get_permalink($post->ID)?>" title="<?=$post->post_title?>"><?=$post->post_title?></a></span>
				<p><?=get_excerpt(60,$post->ID,$post->post_content);?></p>
		</div>
	</div> 
    <? }
    }
    $post = $orig_post;
    wp_reset_query();
    ?>
			</div>
		</div>
	</div>
</div>

<?php 
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 1',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post1 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 2',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post2 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 3',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post3 = get_posts( $posts_args )[0];
?>
<div class="container">
	<div class="row">
		<div class="col-md-12 guncel-sidebar">
			<div class="header col-md-12">
				<span>GÜNCEL HABERLER</span>
			</div>
			<div class="inner col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a href="<?=get_permalink($post1->ID)?>"><?=get_the_post_thumbnail($post1->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a href="<?=get_permalink($post1->ID)?>" title="<?=$post1->post_title?>"><?=$post1->post_title?></a></span>
						<p><?=get_excerpt(60,$post1->ID,$post1->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a href="<?=get_permalink($post2->ID)?>"><?=get_the_post_thumbnail($post2->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a href="<?=get_permalink($post2->ID)?>" title="<?=$post2->post_title?>"><?=$post2->post_title?></a></span>
						<p><?=get_excerpt(60,$post2->ID,$post2->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a  href="<?=get_permalink($post3->ID)?>"><?=get_the_post_thumbnail($post3->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a href="<?=get_permalink($post3->ID)?>" title="<?=$post3->post_title?>"><?=$post3->post_title?></a></span>
						<p><?=get_excerpt(60,$post3->ID,$post3->post_content);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
