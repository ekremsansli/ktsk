<?php
/**
 * Template Name: Kurs Kayıt
 * The template for displaying imagewall blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			<div class="col-md-12">
				<p style="color: #37393d;text-transform: uppercase;text-align: left;margin: 17px 0;display: inline-block;width: 100%;font-weight: 700;font-size: 26px;"><?php echo $_GET['kurs_isim']?></p>
				<?php echo do_shortcode('[dopbsp id="'.$_GET['kurs_id'].'" lang="tr"]')?>
			</div>
		</div>
	</section>
</div>
<?php get_footer();