<?php
/**
 * The template for displaying search form
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */
?><div id="sb-search" class="sb-search"> <span class="sb-icon-search"> </span>
	<form method="get" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="<?php echo get_search_query(); ?>" name="s" id="s">
		<input class="sb-search-submit" type="submit" value="">
	</form>
</div>