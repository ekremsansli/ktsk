<?php
/**
 * Error 404
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>

<section class="content-main clearfix container" >
	<div class="column span-8">
		<article>
			<h2>ERROR 404</h2>
			<h4>nothing found</h4>
		</article>
	</div>

	<div class="column span-4 right-bar">
		<?php get_sidebar( 'right' ); ?>
	</div>
</section>

<?php get_footer(); ?>