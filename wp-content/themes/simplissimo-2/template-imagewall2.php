<?php
/**
 * Template Name: Imagewall Blog 2
 * The template for displaying imagewall blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<?php get_template_part( 'partials/header' , 'imagewall2' ); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			<div class="col-md-8">
				<?php
					    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }

				$args = array(
					"post_type" => "post",
					"paged" => $paged,
					'category_name' => "Organizasyonlar",
					'posts_per_page' => 10
				);

				$wp_query = new WP_Query($args);

				if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
						global $post; ?>
						<?php get_template_part( 'partials/content' , 'list' ); ?>
					<?php endwhile; // while has_post(); ?>

					<?php the_posts_pagination(); ?>
				<?php endif; // if has_post() ?>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4">
				<div class="right-bar">
					<?php get_sidebar( 'organizasyon' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();