<?php
/**
 * The template for displaying post archives
 *
 * @package Layers
 * @since Layers 1.0.0
 */

get_header(); ?>

<section class="content-main archive clearfix container">
	<?php 
	$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $actual_link = split("/",$actual_link);
	if(!is_numeric($actual_link[2])){
		global $wp_query;
	$original_query = $wp_query;
	$wp_query = null;
	
	$page_ids = array();
		    foreach (get_pages() as $value) {
    	$page_ids[] = $value->ID;
    }
if ( is_front_page() ) {
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
				}	$args=array(
    'tag' => $actual_link[2],
    'posts_per_page'=>3,
    "paged" => $paged,
    'post__not_in' => $page_ids,
    'caller_get_posts'=>1
    );
    
	$wp_query = new WP_Query( $args );

	}

	if( have_posts() ) { ?>
		<div <?php layers_center_column_class(); ?>>

			<?php while( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'partials/content' , 'list' ); ?>
			<?php endwhile; // while has_post(); ?>

			<?php the_posts_pagination(); ?>
		</div>
	<?php } else { // if has_post() ?>
		<div class="column span-8 error-posts">
			<h3>Yazı yok.</h3>
		</div>
	<?php } 

	if(is_numeric($actual_link[2])){
$wp_query = null;
$wp_query = $original_query;
wp_reset_postdata();
}
	?>
	<div class="column span-4 right-bar">
		<?php get_sidebar( 'anasayfa' ); ?>
	</div>
</section>

<?php get_footer();