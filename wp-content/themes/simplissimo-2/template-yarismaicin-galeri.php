
<?php
/*
Template Name: Yarisma İcin Galeri
*/
get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<!--======= POST SECTION =========-->
			<div class="col-md-8" style="background:white;margin-bottom: 30px;padding:0;">
				<div class="col-md-2 ys-menu-outer">
					<?php get_template_part( 'partials/content' , 'menu' ); ?>
				</div>
				<div class="col-md-10 ys-content">
					<p class="title"></p>
					<div class="page_content">
						<div class="row" id="galeri">


							
							<?php
											$args = array(
												"post_type" => "post",
												'category_name' => "Yarışma İçin Galeri",
											);
											$wp_query = new WP_Query($args);
											if( $wp_query->have_posts() ) : ?>
												<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
													global $post; 
													$attachments = get_posts( array(
														'post_type' => 'attachment',
														'posts_per_page' => -1,
														'post_parent' => $post->ID
        											));
        											$urls = array();
        											 if ( $attachments ) {
            											foreach ( $attachments as $attachment ) {

                											$urls[] = wp_get_attachment_url( $attachment->ID, 'thumbnail-size', true );
                											
            											}

        											}
												?>
												<div class="col-md-6" style="margin-bottom:20px;">
												<span class="tittle"><?= $post->post_title?></span>
												<a class="fancybox" rel="gallery<?=$post->ID?>" href="<?=$urls[0]?>">
													<img src="<?=$urls[0]?>" style="width:100%;height:auto;float:left;"/>
													<img src="<?=$urls[1]?>" style="width:50%;height:auto;float:left;"/>
													<img src="<?=$urls[2]?>" style="width:50%;height:auto;float:left;"/>
												</a>
												<?php 
													foreach($urls as $url){
												?>
												<a class="fancybox" rel="gallery<?=$post->ID?>" style="display:none;" href="<?=$url?>">
													<img src="<?=$url?>" style="width:100%;height:auto;float:left;"/>
												</a>
												<?php
													}
												?>
												</div>
												<?php endwhile; // while has_post(); ?>
											<?php endif; // if has_post() 
								?>
							
						</div>
					</div>
				</div>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4" style="margin-bottom: 30px;">
				<div class="right-bar">
					<?php get_sidebar( 'yarismaicin' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();