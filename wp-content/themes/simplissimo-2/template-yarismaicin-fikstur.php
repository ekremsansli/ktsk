
<?php
/*
Template Name: Yarisma İcin Fikstür
*/
get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<!--======= POST SECTION =========-->
			<div class="col-md-8" style="background:white;margin-bottom: 30px;padding:0;">
				<div class="col-md-2 ys-menu-outer">
					<?php get_template_part( 'partials/content' , 'menu' ); ?>
				</div>
				<div class="col-md-10 ys-content">
					<p class="title"></p>
					<div class="page_content">
						<div class="row" id="fikstur">
							<div class="col-md-6">

								<select id="ys-sehir-fikstur">
									<option>Şehir Seçiniz</option>
									<?php $categories =  get_categories('child_of=132&hide_empty=0');
										foreach ($categories as $key => $value) {
											if($value->parent == 132)
												echo "<option value=".$value->term_id." data-id='".$value->term_id."'>".$value->name."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<select disabled id="ys-brans-fikstur">
									<option>Branş Seçiniz</option>
								</select>
							</div>
							<div class="col-md-12"  id="kategori-div" style="margin-top:8px;">
								<select id="ys-brans-kategori" disabled>
									<option>Kategori Seçiniz</option>
								</select>
							</div>
							<div class="col-md-12" id="fikstur-content" style="margin-top:20px;">
								<?=get_post(544)->post_content?>
							</div>
						</div>

					</div>
				</div>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4" style="margin-bottom: 30px;">
				<div class="right-bar">
					<?php get_sidebar( 'yarismaicin' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();