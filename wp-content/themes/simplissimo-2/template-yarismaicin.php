
<?php
/*
Template Name: Yarisma İcin Fikstür
*/
get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<!--======= POST SECTION =========-->
			<div class="col-md-8" style="background:white;margin-bottom: 30px;padding:0;">
				<div class="col-md-2 ys-menu-outer">
					<?php get_template_part( 'partials/content' , 'menu' ); ?>
				</div>
				<div class="col-md-10 ys-content">
					<p class="title"></p>
					<div class="page_content">
						<div class="row" id="fikstur" style="display:none;">
							<div class="col-md-6">
								<select id="ys-sehir-fikstur">
									<option>Şehir Seçiniz</option>
									<?php $categories =  get_categories('child_of=132&hide_empty=0');
										foreach ($categories as $key => $value) {
											if($value->parent == 132)
												echo "<option value=".$value->term_id." data-id='".$value->term_id."'>".$value->name."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<select disabled id="ys-brans-fikstur">
									<option>Branş Seçiniz</option>
								</select>
							</div>
							<div class="col-md-12" id="fikstur-content">

							</div>
						</div>
						<div class="row" id="haberler"  style="display:none">
							<div class="col-md-6">
								<select id="ys-sehir-haberler">
									<option>Şehir Seçiniz</option>
									<?php $categories =  get_categories('child_of=140&hide_empty=0');
										foreach ($categories as $key => $value) {
											if($value->parent == 140)
												echo "<option value=".$value->term_id." data-id='".$value->term_id."'>".$value->name."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<select disabled id="ys-brans-haberler">
									<option>Branş Seçiniz</option>
								</select>
							</div>
							<div class="col-md-12" id="haberler-content">
								<?php
									$args = array(
										"post_type" => "post",
										'category_name' => "Yarışma İçin Haber",
									);

									$wp_query = new WP_Query($args);

									if( $wp_query->have_posts() ) : ?>
									<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
										global $post; ?>
										<?php get_template_part( 'partials/content' , 'list' ); ?>
									<?php endwhile; // while has_post(); ?>
									<?php endif; // if has_post() 
								?>
							</div>
						</div>
						<div class="row" id="sonuclar">
							<div class="col-md-6">
								<select id="ys-sehir-sonuclar">
									<option>Şehir Seçiniz</option>
									<?php $categories =  get_categories('child_of=145&hide_empty=0');
										foreach ($categories as $key => $value) {
											if($value->parent == 145)
												echo "<option value=".$value->term_id." data-id='".$value->term_id."'>".$value->name."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<select disabled id="ys-brans-sonuclar">
									<option>Branş Seçiniz</option>
								</select>
							</div>
							<div class="col-md-12" style="margin-top:30px;">
								<table class="table table-hover">
							      <thead>
							        <tr>
							          <th>TARİH</th>
							          <th>ŞEHİR</th>
							          <th>BRANŞ</th>
							          <th>TAKIM 1</th>
							          <th>TAKIM 2</th>
							          <th>SONUÇ</th>
							        </tr>
							      </thead>
							      <tbody>
							        <tr>
							          <?php
											$args = array(
												"post_type" => "post",
												'category_name' => "Yarışma İçin Sonuçlar",
											);
											$wp_query = new WP_Query($args);
											if( $wp_query->have_posts() ) : ?>
												<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
													global $post; ?>
													<tr>
							          					<th scope="row"><?php 
							          						$teams = split('-',$post->post_title);
							          						$d = strtotime(get_post_meta($post->ID)['tarih'][0]);
							          						echo date('d',$d);
							          						echo "/";
							          						echo date('m',$d);
							          						echo "/";
							          						echo date('Y',$d);
							          					?></th>
							          					<td><?=get_post_meta($post->ID)['Şehir'][0]?></td>
							          					<td><?=get_post_meta($post->ID)['Branş'][0]?></td>
							          					<td><?=$teams[0]?></td>
							          					<td><?=$teams[1]?></td>
							          					<td><?=get_post_meta($post->ID)['Sonuç'][0]?></td>
							        				</tr>
												<?php endwhile; // while has_post(); ?>
											<?php endif; // if has_post() 
								?>
							        </tr>
							      </tbody>
							    </table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4" style="margin-bottom: 30px;">
				<div class="right-bar">
					<?php get_sidebar( 'bizkimiz' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();