<?php
/**
 * Template Name: Layers Template
 *
 * This template is used for displaying the Layers page builder
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>

<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			<div class="col-md-8">
				<?php
				do_action('before_layers_builder_widgets');

				// Dynamic Sidebar for this page
				dynamic_sidebar( 'obox-layers-builder-' . $post->ID );

				do_action('after_layers_builder_widgets');
				?>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4">
				<div class="right-bar">
					<?php get_sidebar( 'right' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>