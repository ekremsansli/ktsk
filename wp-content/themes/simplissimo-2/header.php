<?php
	$header_info = simplissimo_get_header_info();
	if ( isset( $post ) ) {
		$side_options = get_post_meta( $post->ID, 'simplissimo_custom_page_side_options', false );
		if ( isset( $side_options[0] ) ) {
			$side_options = $side_options[0];
		}
	}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php wp_head(); ?>
	<script type='text/javascript' src='/wp-content/themes/simplissimo-2/assets/js/swiper.min.js'></script>

</head>
<body <?php body_class(); ?>>
	<?php if ( $header_info['preloader'] ) { ?><div class="loader-icon"><div class="loader load1"></div></div><?php } ?>
	<!-- Page Wrap ===========================================-->
	<div id="wrap"><?php /* Wrap start */ ?>

		<?php if ( isset( $side_options['header-type'] ) && $side_options['header-type'] == 2 ) { ?>
		<!--======= HEADER =========-->
		<header class="sticky header-wall">
			<div class="container">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="top-header">
								<?php if ( ! empty( $header_info['socials'] ) ) { ?><ul class="social_icons">
									<li class="twitter"><a href="mailto:info@ktsk.com.tr" class="zoom popup-with-move-anim"><i class="fa fa-envelope"></i></a></li>
									<?php foreach ( $header_info['socials'] as $social ) { ?>
										<li class="<?php echo esc_attr( str_replace(' ', '-', strtolower( $social["social-title"] ) ) ); ?>"><a href="<?php echo esc_url( $social["social-link"] ); ?>" target="_blank"><i class="<?php echo esc_attr ( $social["social-icon"] ); ?>"></i></a></li>
									<?php } ?>
								</ul><?php } ?>
								<div class="logo"><a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $header_info['logo-light'] ); ?>" alt="Logo"></a></div>
								<?php echo get_search_form(); ?>
							</div>
						</div>
					</div>

					<div class="menu">

						<nav class="navbar-nav nav">

							<!--======= MENU =========-->
							<nav class="navbar navbar-default">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-respo"> <span class="sr-only fa fa-navicon"></span> </button>
								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="nav-respo">
									<?php simplissimo_top_menu('dark'); ?>
								</div>
							</nav>
						</nav>
					</div>
				</div>
			</div>
			<!--======= LOGO =========-->

		</header>
		<?php } else { ?>
		<!--======= HEADER =========-->
		<header class="sticky">

			<!--======= LOGO =========-->

			<div class="container">
				<div class="row">
					<div class="top-header">
						<?php if ( ! empty( $header_info['socials'] ) ) { ?><ul class="social_icons">
							
							<?php foreach ( $header_info['socials'] as $social ) { ?>
								<li class="<?php echo esc_attr( str_replace(' ', '-', strtolower( $social["social-title"] ) ) ); ?>"><a href="<?php echo esc_url( $social["social-link"] ); ?>" target="_blank"><i class="<?php echo esc_attr ( $social["social-icon"] ); ?>"></i></a></li>
							<?php } ?>
						</ul><?php } ?>
						<div class="logo"><a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $header_info['logo'] ); ?>" alt="Logo"></a></div>
						<div id="sb-search" class="sb-search" style="width:auto!important;">
							<div class="rezerv hbtn" style="margin-left:0!important,">
									<p>KURS KAYIT</p>
									<p style="display:none;color:white;" class="rezerv-filter"><a style="color:white;" href="/tag/yetiskin">YETİŞKİN</a> | <a style="color:white;" href="/tag/cocuk">ÇOCUK</a></p>
							</div>
							<div class="kayit hbtn">
								<a href="/saha-rezerve-et">
									<p>SAHA REZERVE ET</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="menu">
				<nav class="navbar-nav nav">
					<!--======= MENU =========-->
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-respo"> <span class="sr-only fa fa-navicon"></span> </button>
						</div>
						<?php simplissimo_top_menu(); ?>
					</nav>
				</nav>
			</div>
		</header>
		<?php } ?>

		<!--======= CONTENT START =========-->
		<div class="content content-margined"><?php /* Content start */ ?>
