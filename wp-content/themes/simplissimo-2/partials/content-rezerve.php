<?php
/**
 * This template is used for displaying posts in post lists
 *
 * @package Layers
 * @since Layers 1.0.0
 */

global $post, $layers_post_meta_to_display;
?>
<div <?php post_class(); ?>>

  <?php
    $post_format = (get_post_format() == true) ? get_post_format():'standard';

    ?>
    <article>
        <ul class="row">
            <li class="col-sm-12">
                <span class="tags"><?php the_tags('<span>','</span><span>','</span>'); ?></span>
                <h4><a href="<?php the_permalink(); ?>" class="tittle"><?php the_title(); ?></a></h4>
                <?php if ( is_single() ) { 
                  the_content();
                } else {
                the_excerpt(); ?>
                  <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
                <?php } ?>
            </li>
        </ul>
    </article>
</div>