<?php
/**
 * This partial is used for displaying three posts after header
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

$categories = ( cs_get_option( 'sidebar-slider' ) ) ? implode( ',', cs_get_option( 'sidebar-slider' ) ) : 0;

$slider_args = array(
	'posts_per_page'   => 3,
	'offset'           => 0,
	'category'         => $categories,
	'orderby'          => 'rand',
	'order'            => 'DESC',
	'post_type'        => 'post',
	'post_status'      => 'publish',
	'suppress_filters' => true
);
$slides = get_posts( $slider_args );
$i = 0;
?>
<div class="banner slide-off">
	<div class="container-fluid">
		<div class="row">
			<?php foreach ($slides as $slide) { ?>
				<?php
					$cats = get_the_category( $slide->ID );
					$cur_cat = $cats[0]->cat_name;
					$i++;
					$cats_n = array();
					foreach ($cats as $cat) {
						$cats_n[] = $cat->cat_name;
					}
					$cats_string = implode( ', ', $cats_n );

					if ( has_post_thumbnail( $slide->ID ) ) {
						$feat_image = wp_get_attachment_image_src( get_post_thumbnail_id( $slide->ID ), 'full' );
						$feat_image = $feat_image[0];
					} else {
						$feat_image = 'http://dummyimage.com/640x800/cccccc/ffffff&text=+++++Post+hasn\'t+image';
					}
				?>
				<div class="bar-wrapp">
					<div class="bar">
						<div class="text-head"> <a href="#." class="showitem toogler showr<?php echo $i; ?>" data-item-id="<?php echo $i; ?>">
							<p><span><?php echo esc_attr( $cur_cat ); ?></span> <?php echo esc_textarea( $slide->post_title ); ?></p>
						</a> <div class="hideitem hidr<?php echo $i; ?>" data-item-id="<?php echo $i; ?>"> </div><a href="<?php get_permalink( $slide->ID ); ?>" class="det-pages"></a> </div>
					</div>
					<div class="banner-coll<?php echo $i; ?> animated fadeInLeft">
						<div class="coll-inner">
							<div class="coll-inner-date">
								<i class="fa fa-clock-o"></i><p><?php echo esc_attr( get_the_time( 'F j, Y', $slide->ID ) ); ?> </p><div class="mini-tags"><i class="fa fa-pencil"></i><p> for </p><span>&ensp; <?php echo esc_attr( $cats_string ); ?>&ensp; </span><p> by <?php echo esc_attr( get_the_author_meta( 'nicename', $slide->post_author ) ); ?></p></div>
							</div>
							<h6><?php echo esc_textarea( $slide->post_title ); ?></h6>
							<?php echo wpautop( esc_textarea( $slide->post_content ) ); ?>
							<a href="<?php echo esc_url( get_permalink( $slide->ID ) ); ?>" class="btn">Read More</a>
							<button class="hideitem hidr<?php echo $i; ?>" data-item-id="<?php echo $i; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/close-icon.png" alt=""></button>
						</div>
					</div>
					<img class="img-responsive" src="<?php echo esc_url( $feat_image ); ?>" alt="Featured image">
				</div>
			<?php } ?>
		</div>
	</div>
</div>