<?php
/**
 * This partial is used for displaying single post (or page) content
 *
 * @package Layers
 * @since Layers 1.0.0
 */

function print_post( array $single ) {
	print '	<a class="img-wall-block" href="' . $single['url'] . '">
		<img src="' . $single['image'] . '" alt="image wall">
		<div class="wall-title">
			<span>' . $single['category'] . '</span>
			<h6>' . $single['title'] . '</h6>
		</div>
	</a>';
}

$category = 129;

$posts_args = array(
	'posts_per_page'   => -1,
	'offset'           => 0,
	'category'         => $category,
	'orderby'          => 'post_date',
	'order'            => 'ASC',
	'post_type'        => 'post',
	'post_status'      => 'publish',
	'suppress_filters' => true
);

$temp_posts = get_posts($posts_args);

$posts = array();
$i = 0;
foreach ( $temp_posts as $tpost ) {
	$i++;
	$tags = wp_get_post_tags( $tpost->ID );
	$attachments = get_posts( array(
		'post_type' => 'attachment',
		'posts_per_page' => -1,
		'post_parent' => $tpost->ID,
		'exclude'     => get_post_thumbnail_id($tpost->ID)
	));
		$img_id = get_post_thumbnail_id( $tpost->ID );
		$img_url = $attachments[0]->guid;

	$cat_ids =0;
	foreach (wp_get_post_categories($tpost->ID) as $key => $value) {
		if($value != 39)
			$cat_ids = $value;
	}
	$posts[$i]['category'] = get_post_meta($tpost->ID,'eglence_icin_slider_kategori')[0];
	$posts[$i]['url']	= get_permalink( $tpost->ID );
	$posts[$i]['image']	= esc_url( $img_url );
	if ( isset( $tags[0] ) ) {
		$posts[$i]['tag']	= $tags[0]->name;
	}
	$posts[$i]['title']	= $tpost->post_title;
}

?>
<div class="containe-fluid" style="margin-top:-50px;">
	<div class="row">
		<?php if ( !empty( $posts ) ) { ?>
		<div class="wall-baner2 swiper-container" style="margin-bottom: 40px;">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="imgwall">
						<div class="w-40">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<?php if ( isset( $posts[1] ) && !empty( $posts[1] ) ) { print_post( $posts[1] ); } ?>
									<?php if ( isset( $posts[2] ) && !empty( $posts[2] ) ) { print_post( $posts[2] ); } ?>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<?php if ( isset( $posts[3] ) && !empty( $posts[3] ) ) { print_post( $posts[3] ); } ?>
								</div>
							</div>
						</div>
						<div class="w-60">
							<div class="row">
								<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
									<?php if ( isset( $posts[4] ) && !empty( $posts[4] ) ) { print_post( $posts[4] ); } ?>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<?php if ( isset( $posts[5] ) && !empty( $posts[5] ) ) { print_post( $posts[5] ); } ?>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<?php if ( isset( $posts[6] ) && !empty( $posts[6] ) ) { print_post( $posts[6] ); } ?>
								</div>
								<div class="col-lg-8 col-md-12">
									<?php if ( isset( $posts[7] ) && !empty( $posts[7] ) ) { print_post( $posts[7] ); } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } else { ?>
			<span class="imagewall-notice">Please add images to wall on the page <a href="<?php echo admin_url( 'admin.php?page=cs-framework' ); ?>">Theme options</a> > Tab "Blog" > Section "Imagewall"</span>
		<?php } ?>
	</div>
</div>