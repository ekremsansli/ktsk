<?php
/**
 * Post format gallery
 *
 * @package Layers
 * @since Layers 1.0.0
 */

$gallery = get_post_gallery_images( $post );
if ( !empty( $gallery ) ) {
	$post_options = get_post_meta( $post->ID, 'simplissimo_custom_post_options', false );
	if ( isset( $post_options[0] ) ) {
		$post_options = $post_options[0];
		$gallery_type = $post_options['gallery-type'];
	} else {
		$gallery_type = 0;
	}

	if ( $gallery_type == 2 ) { // if Gallery type is Slider with thumbs ?>
	<div id="slider" class="flexslider">
		<ul class="slides">
			<?php $ii = 1; foreach($gallery as $img) {
				if ($ii <= 6) :?>
				<li><img class="img-responsive" src="<?php echo esc_url(str_replace('-150x150','',$img));?>" alt="Image <?php echo $ii; ?>" ></li>
			<?php endif;?>
			<?php $ii++;} ?>
			<!-- items mirrored twice, total of 12 -->
		</ul>
	</div>
	<div id="carousel" class="flexslider">
		<ul class="slides">
			<?php $ii = 1; foreach($gallery as $img) {
				if ($ii <= 6) :?>
				<li><img class="img-responsive" src="<?php echo esc_url(str_replace('-150x150','',$img));?>" alt="Image <?php echo $ii; ?>" ></li>
			<?php endif;?>
			<?php $ii++;} ?>
			<!-- items mirrored twice, total of 12 -->
		</ul>
	</div>
	<?php } elseif ( $gallery_type == 3 ) { // if Gallery type is Carousel ?>
		<div class="gallery-post">
			<div id="owl-gallery">
				<?php $ii = 1; foreach($gallery as $img) {
					if ($ii <= 6) :?>
					<div class="item"><img class="img-responsive" src="<?php echo esc_url(str_replace('-150x150','',$img));?>" alt="Image <?php echo $ii; ?>" ></div>
					<?php endif;?>
				<?php $ii++;} ?>
			</div>
		</div>
	<?php } else { // if Gallery type is Simple Gallery ?>
		<div class="multi-images post-gallery">
			<ul>
			<?php $ii = 1; foreach($gallery as $img) {
				if ($ii <= 4) :?>
				<li class="column span-6"> <img class="img-responsive" src="<?php echo esc_url(str_replace('-150x150','',$img));?>" alt="Image <?php echo $ii; ?>">
					<div class="img-over"> <a class="image-popup" href="<?php echo esc_url(str_replace('-150x150','',$img));?>" title="Image <?php echo $ii; ?>"> <i class="fa fa-expand"></i> </a></div>
				</li>
				<?php endif;?>
			<?php $ii++;} ?>
			</ul>
		</div>
	<?php } ?>
<?php } ?>