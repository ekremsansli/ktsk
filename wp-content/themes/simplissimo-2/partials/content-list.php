<?php
/**
 * This template is used for displaying posts in post lists
 *
 * @package Layers
 * @since Layers 1.0.0
 */

global $post, $layers_post_meta_to_display;
?>
<div <?php post_class(); ?>>

	<?php
    $post_format = (get_post_format() == true) ? get_post_format():'standard';
	if ($post_format == 'gallery'){
		get_template_part('partials/format', 'gallery' );
	} else if ($post_format == 'video'){
		if (!is_single()) the_content();
	} else {
	?>

		<?php if ( has_post_thumbnail() ) { ?>
        <div class="multi-images first-post">
            <ul>
              <li>
                <a href="<?php the_permalink(); ?>">
                <?php echo "<img width='785' height='492' src='".wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),array(785,492))[0]."'/>";
                $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));?>
                </a>
              </li>
            </ul>
        </div>
        <?php }

	}?>

    <article>
        <ul class="row">
            <!--======= POST DATE =========-->
            <?php 

              if(get_post_meta($post->ID)['tarih'][0] != ""){
                $tarih = strtotime(get_post_meta($post->ID)['tarih'][0]);
            ?>
                
            <li class="col-sm-3">
                <div class="date"><span class="day"><?php echo strtoupper( zamantr(date('l',$tarih)) ); ?></span><span class="big"><?php echo date('d',$tarih); ?></span><span><?php echo zamantr(date('F Y',$tarih)); ?></span></div>
            </li>
            <?php    
              }
              else{
                ?>
                <li class="col-sm-3">
                    <div class="date"><span class="day"><?php strtoupper( the_time('D') ); ?></span><span class="big"><?php the_time('d'); ?></span><span><?php the_time('F Y'); ?></span></div>
                </li>
                <?php
              }  

             ?>
            <!--======= POST TEXT =========-->
            <li class="col-sm-9">

                <span class="tags"><?php the_tags('<span>','</span><span>','</span>'); ?></span>
                <h4><a href="<?php the_permalink(); ?>" class="tittle"><?php the_title(); ?></a></h4>
                <?php if ( is_single() ) { the_content(); } else {
                the_excerpt(); ?>
                <a class="btn" href="<?php the_permalink(); ?>">DEVAMINI OKU</a>
                <?php } ?>
            </li>
        </ul>
    </article>
</div>


