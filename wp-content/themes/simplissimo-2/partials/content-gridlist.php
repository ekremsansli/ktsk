<?php
/**
 * This template is used for displaying posts in post lists
 *
 * @package Layers
 * @since Layers 1.0.0
 */

global $post, $layers_post_meta_to_display;

if ( has_post_thumbnail() ) {
    $img_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
} else {
    $img_url = 'http://dummyimage.com/360x270/#ccc/ffffff.png&text=Post+hasn\'t+image';
}
?>

<div <?php post_class( 'col-md-4 col-sm-4 grid-item' ); ?>>
    <div class="post">
        <a href="<?php the_permalink(); ?>" class="tittle">
            <img class="img-responsive" src="<?php echo esc_url( $img_url ); ?>">
        </a>
        <article>
            <span class="tags"><?php the_tags('<span>','</span><span>','</span>'); ?></span>
            <h4><a href="<?php the_permalink(); ?>" class="tittle"><?php the_title(); ?></a></h4>
            <ul class="row">
                <li class="col-sm-12 col-md-12">
                    <?php the_excerpt(); ?>
                </li>
            </ul>
        </article>
    </div>
</div>