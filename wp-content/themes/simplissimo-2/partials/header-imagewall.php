<?php
/**
 * This partial is used for displaying single post (or page) content
 *
 * @package Layers
 * @since Layers 1.0.0
 */

function print_post( array $single ) {
	print '	<a class="img-wall-block" href="' . $single['url'] . '">
		<img src="' . $single['image'] . '" alt="image wall">
		<div class="wall-title">
			<span>' . $single['tag'] . '</span>
			<h6>' . $single['title'] . '</h6>
		</div>
	</a>';
}

$category = cs_get_option( 'imagewall-category' );


$posts_args = array(
	'posts_per_page'   => -1,
	'offset'           => 0,
	'category'         => $category,
	'orderby'          => 'post_date',
	'order'            => 'ASC',
	'post_type'        => 'post',
	'post_status'      => 'publish',
	'suppress_filters' => true
);

$temp_posts = get_posts( $posts_args );

$posts = array();
$i = 0;
foreach ( $temp_posts as $tpost ) {
	$i++;
	
	$tags = wp_get_post_tags( $tpost->ID );
	if ( has_post_thumbnail( $tpost->ID ) ) {
		$img_id = get_post_thumbnail_id( $tpost->ID );
		$img_url = wp_get_attachment_url( $img_id, array(300,300),true);
	} else {
		continue;
	}
	$posts[$i]['id']	= $tpost->ID;
	$posts[$i]['url']	= get_permalink( $tpost->ID );
	$posts[$i]['image']	= esc_url( $img_url );

	if ( isset( $tags[0] ) ) {
		$posts[$i]['tag']	= $tags[0]->name;
	}
	$posts[$i]['title']	= $tpost->post_title;
}

?>
<div class="containe-fluid">
	<div class="row">
		<?php 
			foreach ( $posts as $post ) {
		?>
		<div class="bar-wrapp">
			<div class="bar">
				<div class="text-head">
					<a href="<?php if($post['id']==44){echo '/hakkimizda-2';}else if($post['id'] == 47){echo '/yarisma-icin-fikstur';}else if($post['id'] == 50){echo '/baglarbasi-korusu-spor-tesisleri/';}?>" class="showitem toogler showr1" data-item-id="1">
						<p style="<?php if($post['id']==44){echo 'margin-left:9px;';}?>"><?=$post['title']?></p>
					</a>
					<div class="hideitem hidr1" data-item-id="1"></div>
					<a href="<?=$post['url']?>" class="det-pages"></a> 
				</div>
			</div>
			<a href="<?php if($post['id']==44){echo '/hakkimizda-2';}else if($post['id'] == 47){echo '/yarisma-icin-fikstur';}else if($post['id'] == 50){echo '/baglarbasi-korusu-spor-tesisleri/';}?>"><img class="img-responsive" style="height:500px!important;" src="<?=$post['image']?>" alt="<?=$post['title']?>"></a>
		</div>
		<?php
			}
		?>
	</div>
</div>
<?php 
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Üst Güncel 1',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post1 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Üst Güncel 2',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post2 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Üst Güncel 3',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post3 = get_posts( $posts_args )[0];

?>
<div class="container">
	<div class="row">
		<?php 
		$style="";
		$reklam = false;
		$img_url = wp_get_attachment_url( get_post_thumbnail_id(583) );
			if($img_url != ""){
				$style = "display:none;";
				$reklam = true;

			}
		?>
		<?php 
			
			if($img_url != "")
				echo '<div class="col-md-12 guncel-anasayfa"><a target="_blank" href="'.get_post_meta(583)["reklam_url"][0].'"><img style="width:100%;height:auto;" src="'.$img_url.'" /></a></div>';
		?>
		
		<div class="col-md-12 guncel-anasayfa" style="<?=$style?>">

			<div class="header col-md-2">
				<span>GÜNCEL HABERLER</span>
			</div>
			<div class="inner col-md-12 col-sm-12 col-xs-12">
				<div class="gpost col-md-4 col-sm-4 col-xs-12">
					<div class="gimg col-sm-6"><a href="<?=get_permalink($post1->ID)?>"><?=get_the_post_thumbnail($post1->ID,array(150,150));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post1->ID)?>" title="<?=$post1->post_title?>"><?=$post1->post_title?></a></span>
						<p><?=get_excerpt(180,$post1->ID,$post1->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-4 col-sm-4 col-xs-12">
					<div class="gimg col-sm-6"><a href="<?=get_permalink($post2->ID)?>"><?=get_the_post_thumbnail($post2->ID,array(150,150));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post2->ID)?>" title="<?=$post2->post_title?>"><?=$post2->post_title?></a></span>
						<p><?=get_excerpt(180,$post2->ID,$post2->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-4 col-sm-4 col-xs-12">
					<div class="gimg col-sm-6"><a href="<?=get_permalink($post3->ID)?>"><?=get_the_post_thumbnail($post3->ID,array(150,150));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post3->ID)?>" title="<?=$post3->post_title?>"><?=$post3->post_title?></a></span>
						<p><?=get_excerpt(180,$post3->ID,$post3->post_content);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

