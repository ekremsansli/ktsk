
<?php
/*
Template Name: Kupa İçin
*/
get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">
			<!--======= POST SECTION =========-->
			<div class="col-md-8" style="background:white;margin-bottom: 30px;padding:0;">
				<div class="col-md-2 ys-menu-outer" style="display:none;">
					<ul class="ys-menu">
						<li><a href="javascript:void(0);" id="catName"></a></li>
						<li><a href="javascript:void(0);" class="lili fikstur-li">Fikstür</a></li>
						<li><a href="javascript:void(0);" class="lili haber-li">Haberler</a></li>
						<li><a href="javascript:void(0);" class="lili sonuc-li">Sonuçlar</a></li>
						<li><a href="javascript:void(0);" class="lili puan-li">Puan Durumu</a></li>
						<li><a href="javascript:void(0);" class="lili galeri-li">Galeri</a></li>
					</ul>
				</div>
				<div class="col-md-12 ys-content" style="margin-top:29px;">
					<p class="title" id="pageTitle">Kupa İçin</p>
					<div class="page-content">
						<div class="startDiv">
							<div class="row">
								<div class="col-md-6">
									<select id="ks-brans" style="margin-bottom:30px;">
										<option>Branş Seçiniz</option>
										<?php $categories =  get_categories('child_of=151&hide_empty=0');
											foreach ($categories as $key => $value) {
												if($value->parent == 151)
													echo "<option value=".$value->term_id." data-id='".$value->term_id."'>".$value->name."</option>";
											}
										?>
									</select>
								</div>
								<div class="col-md-6">
									<select id="ks-lig" disabled style="margin-bottom:30px;">
										<option>Lig Seçiniz</option>
									</select>
								</div>
							</div>
							<div class="container">
							<div class="row" id="liglerListe">
								<?php 
								

									$categories =  get_categories('child_of=151&hide_empty=0');
									foreach ($categories as $key => $value) {
										if($value->description == "false"){
											$string = "<div class='col-md-4 grid-item' style='text-align:center;'>";
    										$string .= "<a href='javascript:void(0);'data-catid='".$value->term_id."' data-catname='".$value->name."' class='open-league'><img style='width:100%;height:auto;' src='/wp-content/themes/simplissimo-2/assets/images/kupaIcin.jpg'/></a>";
    										$string .= "<span class='tags' style='clear:left;float:none;'><span>".get_top_parent($value->term_id)."</span></span>";
    										$string .= "<p class='tittle' style='text-align:center!important;'><a href='javascript:void(0);' class='open-league' data-catid='".$value->term_id."' data-catname='".$value->name."'>".$value->name."</a></p>";
    										$string .= "</div>";
    										echo $string;
										}		
									} 	
								?>
							</div>
							</div>
						</div>
						<div class="leagueDiv" style="display:none;">
							<div class="fikstur-div contentDiv"></div>
							<div class="haber-div contentDiv"></div>
							<div class="sonuc-div contentDiv" style="display:none;">
								<table class="table table-hover">
							      <thead>
							        <tr>
							          <th>TARİH</th>
							          <th>ŞEHİR</th>
							          <th>BRANŞ</th>
							          <th>TAKIM 1</th>
							          <th>TAKIM 2</th>
							          <th>SONUÇ</th>
							        </tr>
							      </thead>
							      <tbody>
							      	
							      </tbody>
							    </table>
							</div>
							<div class="puan-div contentDiv">
							</div>
							<div class="galeri-div contentDiv" style="display:none;">
								<?php
											$args = array(
												"post_type" => "post",
												'category_name' => "Kupa İçin Galeri",
											);
											$wp_query = new WP_Query($args);
											if( $wp_query->have_posts() ) : ?>
												<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
													global $post; 
													$attachments = get_posts( array(
														'post_type' => 'attachment',
														'posts_per_page' => -1,
														'post_parent' => $post->ID
        											));
        											$urls = array();
        											 if ( $attachments ) {
            											foreach ( $attachments as $attachment ) {

                											$urls[] = wp_get_attachment_url( $attachment->ID, 'thumbnail-size', true );
                											
            											}

        											}
												?>
												<div class="col-md-6" style="margin-bottom:20px;">
												<span class="tittle"><?= $post->post_title?></span>
												<a class="fancybox" rel="gallery<?=$post->ID?>" href="<?=$urls[0]?>">
													<img src="<?=$urls[0]?>" style="width:100%;height:auto;float:left;"/>
													<img src="<?=$urls[1]?>" style="width:50%;height:auto;float:left;"/>
													<img src="<?=$urls[2]?>" style="width:50%;height:auto;float:left;"/>
												</a>
												<?php 
													foreach($urls as $url){
												?>
												<a class="fancybox" rel="gallery<?=$post->ID?>" style="display:none;" href="<?=$url?>">
													<img src="<?=$url?>" style="width:100%;height:auto;float:left;"/>
												</a>
												<?php
													}
												?>
												</div>
												<?php endwhile; // while has_post(); ?>
											<?php endif; // if has_post() 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4" style="margin-bottom: 30px;">
				<div class="right-bar">
					<?php get_sidebar( 'kupaicin' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();