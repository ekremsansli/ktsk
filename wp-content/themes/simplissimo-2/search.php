<?php
/**
 * The template for displaying a single post
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<?php if( have_posts() ) : ?>
	<section id="post-<?php the_ID(); ?>" <?php post_class( 'content-main clearfix container' ); ?>>
		<div <?php layers_center_column_class(); ?>>
			<?php while( have_posts() ) : the_post(); ?>
				<article <?php post_class( 'container' ); ?>>
					<?php get_template_part( 'partials/content', 'list' ); ?>
					<?php comments_template(); ?>
				</article>
			<?php endwhile; // while has_post(); ?>
		</div>

		<div class="column span-4 right-bar">
			<?php get_sidebar( 'right' ); ?>
		</div>
	</section>

<?php else : // if has_post() ?>

	<section class="content-main clearfix container" >
		<div class="column span-8 posts-error">
			<h2>Nothing found for "<?php echo get_search_query(); ?>"</h2>
			<h3>You can go to <a href="<?php echo esc_url( home_url() ); ?>">Home Page</a> or try to use another words to search...</h3>
		</div>

		<div class="column span-4 right-bar">
			<?php get_sidebar( 'right' ); ?>
		</div>
	</section>

<?php endif; // if has_post() ?>

<?php get_footer(); ?>