/*-----------------------------------------------------------------------------------*/
/* 		Main Js Start
/*-----------------------------------------------------------------------------------*/
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}
(function($) { "use strict";

	/*========== Page Loader start ================*/
	$(window).load(function() {
		$.each($('#wp-calendar').find('a').parent(),function(i,v){
			$(v).css('background','red');
		})
		// Animate loader off screen
		$(".loader").fadeOut();
		/*will fade out the whole DIV that covers the website.*/
		$(".loader-icon").delay(100).fadeOut("slow");
		panel();

		var $container = $('.grid-isotope');
		// init
		$container.isotope({
			// options
			itemSelector: '.grid-item'
		});
	});
	function panel(){
		$('.show-panel').click(function () {
			$('.panel').slideToggle();
		});
	}
	/*========== Page Loader end ================*/

	$(document).ready(function(){ // document.ready start
		// var $container = $('.grid-isotope');
		// // init
		// $container.isotope({
		// 	// options
		// 	itemSelector: '.grid-item'
		// });
	}); // document.ready end
	/*-----------------------------------------------------------------------------------*/
	/* 		Magnific Popup
	/*-----------------------------------------------------------------------------------*/

	$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: true,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
	});
	$('.popup-with-move-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: true,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
	});
	$("#owl-gallery").owlCarousel({
		   navigation : true, // Show next and prev buttons
		   slideSpeed : 300,
		   paginationSpeed : 400,
		   singleItem:true,
		   autoPlay:true,
		   mouseDrag : false,
		   rewindSpeed : 1000,
		   navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		   transitionStyle : "fade"

	  });
	$('.image-popup').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			enabled: true,
			duration: 3000,
			image: {
			verticalFit: true
			}
		});
	$('.popup-vedio').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: true,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
	});
	$('.popup-vedio').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: true,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
	});

	/*-----------------------------------------------------------------------------------*/
	/* 		Swiper Slide
	/*-----------------------------------------------------------------------------------*/

	if ($('.wall-baner').length){
	var wall_container;
		 $(function(){
			 wall_container = $('.wall-baner').swiper({
				slidesPerView: 1,
				calculateHeight: true,
				speed: 1000,
				loop: true,
				resistance: false,
				grabCursor: true
			});
			 $('.wall-baner2').swiper({
				slidesPerView: 1,
				calculateHeight: true,
				speed: 1000,
				loop: true,
				resistance: false,
				grabCursor: true
			});
		});
	}

	/*-----------------------------------------------------------------------------------*/
	/* 		Menu On Click
	/*-----------------------------------------------------------------------------------*/

	$('.dropdown a > .fa').on( "click", function() {
		var LinkThis = $(this).parent().parent();
		if (LinkThis.find('.dropdown-menu').hasClass('slideUp')) {
			LinkThis.find('.dropdown-menu').removeClass('slideUp');
		}else {
			$('.dropdown .dropdown-menu').removeClass('slideUp');
			LinkThis.find('.dropdown-menu').addClass('slideUp');
		}
		return false;
	});

	/*-----------------------------------------------------------------------------------*/
	/* 		Baner Click
	/*-----------------------------------------------------------------------------------*/

	$( ".showitem" ).live( "click", function(event) {
		event.preventDefault();
		var item_id = $(this).attr('data-item-id');
		$( ".banner-coll" + item_id ).first().show( "fast", function showNext() {
			$( this ).next( ".banner-coll" + item_id ).show( "fast", showNext );
		});
		$( this ).removeClass( 'showitem' ).addClass( 'hideitem' );
	});

	$( ".hideitem" ).live( "click", function(event) {
		event.preventDefault();
		var item_id = $(this).attr('data-item-id');
		$( ".banner-coll" + item_id ).fadeOut( 200 );
		$( '.toogler.showr' + item_id ).removeClass( 'hideitem' ).addClass( 'showitem' );
	});

	/*-----------------------------------------------------------------------------------*/
	/* 		Back to Top
	/*-----------------------------------------------------------------------------------*/

	$(window).scroll(function(){
	 if($(window).scrollTop() > 1000){
		  $("#back-to-top");
		} else{
		  $("#back-to-top");
		}
	});
	$('#back-to-top, .back-to-top').click(function() {
		  $('html, body').animate({ scrollTop:0 }, '3000');
		  return false;
	});

	/*-----------------------------------------------------------------------------------*/
	/* 		Google Map
	/*-----------------------------------------------------------------------------------*/

	 function initialize(obj) {
		var lat = $('#'+obj).attr("data-lat");
		var lng = $('#'+obj).attr("data-lng");
		var contentString = $('#'+obj).attr("data-string");
		var myLatlng = new google.maps.LatLng(lat,lng);
		var map, marker, infowindow;
		var image = 'images/marker.png';
		var zoomLevel = parseInt($('#'+obj).attr("data-zoom"));

		var mapOptions = {
			zoom: zoomLevel,
			disableDefaultUI: true,
			center: myLatlng,
			scrollwheel: false,
			mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		}

		map = new google.maps.Map(document.getElementById(obj), mapOptions);



		infowindow = new google.maps.InfoWindow({
			content: contentString
		});


		marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
		
	}

	/*-----------------------------------------------------------------------------------*/
	/* 		Window load
	/*-----------------------------------------------------------------------------------*/

	$(window).load(function() {
	  if($('#map-canvas-contact').length==1){
		 initialize('map-canvas-contact');
		}

	  $('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: true,
		slideshow: true,
		itemWidth: 130,
		itemMargin: 5,
		asNavFor: '#slider'
	  });

	  $('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: true,
		slideshow: true,
		sync: "#carousel"
	  });
	  $('#slider-svk').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: true,
		slideshow: true,
		slideshowSpeed: 3000,
		prevText: "",
		nextText: ""
	  });

 	$(".slider-bk").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
	 	items:4,   
      navigation:true,
      pagination:false,
      navigationText:["",""]
  	});
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		margin:0,
		padding:0
	});
});

	

	jQuery(document).ready(function($) {
		// instantiate the accordion
		$('#example1').accordionSlider({
			width: 1920,
			height: 850,
			closePanelsOnMouseOut: true,
			shadow: false,
			responsiveMode: 'auto',
			panelDistance: 0,
			autoplay: false,
			mouseWheel: false
		});
	});
$(document).ready(function() {
	$.each($('.bk-menu').find('a'),function(i,v){
		var ll = window.location.pathname.replace(new RegExp('/', 'g'), '');
		if($(v).attr('href').indexOf(ll) != -1){
			if($(v).parent().parent().attr('id') == "sub-ul"){
				$('#ss').addClass('active').css('font-weight','bold').css('font-size','17px');
			}
			$(v).addClass('active');
		}
	});
	$.each($('.ys-menu').find('a'),function(i,v){
		var ll = window.location.pathname.replace(new RegExp('/', 'g'), '');
		if($(v).attr('href').indexOf(ll) != -1){
			if($(v).parent().parent().attr('id') == "sub-ul"){
				$('#ss').addClass('active').css('font-weight','bold').css('font-size','17px');
			}
			$(v).addClass('active');
		}
	});
    function close_accordion_section() {
        $('.accordion .accordion-section-title').removeClass('active');
        $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }
 	$('.rezerv').click(function(){
 		if($('.rezerv').find('p:eq(0)').is(':visible')){
 			$('.rezerv').find('p:eq(0)').hide();
 			$('.rezerv').find('p:eq(1)').show();
 		}

 			
 	});
    $('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).attr('href');
 
        if($(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();
 
            // Add active class to section title
            $(this).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        }
 
        e.preventDefault();
    });
    $('#ys-sehir-fikstur').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter", cat: catID },
        		success: function(response) {
            		var json = JSON.parse(response);
            		$('#ys-brans-fikstur').html('<option>Branş Seçiniz</option>');
            		$.each(json,function(key,value){
            			if(value.parent == catID)
            				$('#ys-brans-fikstur').append('<option value="'+value.term_id+'" data-son ="'+value.description+'">'+value.name+"</option>");
            		})
            		$('#ys-brans-fikstur').removeAttr('disabled');
            		return false;
        	}
    	});
    });

    $('#ys-brans-fikstur').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	if($(this).find('option:selected').attr('data-son') == "true")
    	{
    		jQuery.ajax({
	        	type: 'POST',
	        	url: ajaxurl,
	        	data: {"action": "load-filter2", cat: catID },
	        		success: function(response) {
	        			var json = JSON.parse(response)[0];
	            		$('#fikstur-content').html(json.post_content);
	            		$('#kategori-div').hide();
	            		return false;
	        	}
    		});
    	}
    	else{
    		$('#kategori-div').show();
    		var catArr = [];
    		jQuery.ajax({
	        	type: 'POST',
	        	url: ajaxurl,
	        	data: {"action": "load-filter", cat: catID },
	        		success: function(response) {
	        			var json = JSON.parse(response);
	            		$.each(json,function(key,value){
	            			if($('#parent_'+value.parent).length > 0)
	            				$('#parent_'+value.parent).append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			else{
	            				if(value.description == "false")
	            					$('#ys-brans-kategori').append("<optgroup id='parent_"+value.term_id+"' label='"+value.name+"'></optgroup>");
	            				else
	            					$('#ys-brans-kategori').append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			}
	            		});
	            		$('#ys-brans-kategori').removeAttr('disabled');
	            		return false;
	        	}
    		});
    	}
    	
    });
	$('#ys-brans-kategori').change(function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter2", cat: catID },
        		success: function(response) {
        			if(JSON.parse(response).length > 0){
        				var json = JSON.parse(response)[0];
            			$('#fikstur-content').html(json.post_content);
            			return false;
        			}
        			
        	}
    	});
	});

    $('#ys-sehir-haberler').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter", cat: catID },
        		success: function(response) {
            		var json = JSON.parse(response);
            		$('#ys-brans-haberler').html('<option>Branş Seçiniz</option>');
            		$.each(json,function(key,value){
            			if(value.parent == catID)
            				$('#ys-brans-haberler').append('<option value="'+value.term_id+'" data-son="'+value.description+'">'+value.name+"</option>");
            		})
            		$('#ys-brans-haberler').removeAttr('disabled');
            		return false;
        	}
    	});
    });
var weekday=new Array(7);
weekday[0]="Pazartesi";
weekday[1]="Salı";
weekday[2]="Çarşamba";
weekday[3]="Perşembe";
weekday[4]="Cuma";
weekday[5]="Cumartesi";
weekday[6]="Pazar";
var months=new Array(12);
months[0]="Ocak";
months[1]="Şubat";
months[2]="Mart";
months[3]="Nisan";
months[4]="Mayıs";
months[5]="Haziran";
months[6]="Temmuz";
months[7]="Ağustos";
months[8]="Eylül";
months[9]="Ekim";
months[10]="Kasım";
months[11]="Aralık";

$('#ys-brans-kategori-haberler').change(function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter2", cat: catID },
        		success: function(response) {
        			var json = JSON.parse(response);
        			$('#haberler-content').html('');
        			$.each(json,function(key,value){
        				var tarih = new Date(value.post_date);
        				var string = "<div class='post-"+value.ID+" post'>";
        				string += "<ul><a href="+value.permalink+"><li> <img width='785' height='492' src="+value.thumbnail+"></a></li></ul>";
        				string += "<article><ul class='row'> <li class='col-sm-3'><div class='date'><span class='day'>"+weekday[tarih.getDay()-1]+"</span><span class='big'>"+tarih.getDate()+"</span><span>"+months[tarih.getMonth()]+" "+tarih.getFullYear()+"</span></div></li><li class='col-sm-9'><span class='tags'>";
        				$.each(value.tagss,function(a,b){
        					string += '<span><a href="/tag/'+b.slug+'/" rel="tag">'+b.name+'</a></span>';
        				});
        				string += '</span><h4><a href="'+value.permalink+'" class="tittle">'+value.post_title+'</a></h4>';
        				string += '<p class="excerpt">'+value.post_content.substr(0,250)+' [...]</p>';
        				string += '<a class="btn" href="'+value.permalink+'">DEVAMINI OKU</a></li>';
        				string += '</ul></article></div>';
        				$('#haberler-content').append(string);
	        			});
            		
            		return false;
        	}
    		});
	});
    $('#ys-brans-haberler').change(function(){
    	   var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	if($(this).find('option:selected').attr('data-son') == "true")
    	{

    		jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter2", cat: catID },
        		success: function(response) {
        			var json = JSON.parse(response);
        			$('#haberler-content').html('');
        			$.each(json,function(key,value){
        				var tarih = new Date(value.post_date);
        				var string = "<div class='post-"+value.ID+" post'>";
        				string += "<ul><a href="+value.permalink+"><li> <img width='785' height='492' src="+value.thumbnail+"></a></li></ul>";
        				string += "<article><ul class='row'> <li class='col-sm-3'><div class='date'><span class='day'>"+weekday[tarih.getDay()-1]+"</span><span class='big'>"+tarih.getDate()+"</span><span>"+months[tarih.getMonth()]+" "+tarih.getFullYear()+"</span></div></li><li class='col-sm-9'><span class='tags'>";
        				$.each(value.tagss,function(a,b){
        					string += '<span><a href="/tag/'+b.slug+'/" rel="tag">'+b.name+'</a></span>';
        				});
        				string += '</span><h4><a href="'+value.permalink+'" class="tittle">'+value.post_title+'</a></h4>';
        				string += '<p class="excerpt">'+value.post_content.substr(0,250)+' [...]</p>';
        				string += '<a class="btn" href="'+value.permalink+'">DEVAMINI OKU</a></li>';
        				string += '</ul></article></div>';
        				$('#haberler-content').append(string);
        				$('#kategori-div-haberler').hide();
        			});
            		
            		return false;
        	}
    		});
    	}
    	else{
    		$('#kategori-div-haberler').show();
    		var catArr = [];
    		jQuery.ajax({
	        	type: 'POST',
	        	url: ajaxurl,
	        	data: {"action": "load-filter", cat: catID },
	        		success: function(response) {
	        			var json = JSON.parse(response);
	            		$.each(json,function(key,value){
	            			if($('#parent_'+value.parent).length > 0)
	            				$('#parent_'+value.parent).append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			else{
	            				if(value.description == "false")
	            					$('#ys-brans-kategori-haberler').append("<optgroup id='parent_"+value.term_id+"' label='"+value.name+"'></optgroup>");
	            				else
	            					$('#ys-brans-kategori-haberler').append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			}
	            		});
	            		$('#ys-brans-kategori-haberler').removeAttr('disabled');
	            		return false;
	        	}
    		});
    	}
    });
	    $('#ys-sehir-sonuclar').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter", cat: catID },
        		success: function(response) {
            		var json = JSON.parse(response);
            		$('#ys-brans-sonuclar').html('<option>Branş Seçiniz</option>');
            		$.each(json,function(key,value){
            			if(value.parent == catID)
            				$('#ys-brans-sonuclar').append('<option value="'+value.term_id+'" data-son="'+value.description+'">'+value.name+"</option>");
            		})
            		$('#ys-brans-sonuclar').removeAttr('disabled');
            		return false;
        	}
    	});
    });
    $('#ys-brans-sonuclar').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	if($(this).find('option:selected').attr('data-son') == "true")
    	{
            jQuery.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {"action": "load-filter2", cat: catID },
                    success: function(response) {
                        var json = JSON.parse(response);
                        $('#sonuclar table tbody').html('');
                        $.each(json,function(key,value){
                            var teams = value.post_title.split("-");
                            $('#sonuclar table tbody').append("<tr><th scope='row'><a href='"+value.permalink+"'>"+value.tarih+"</a></th><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(value.sehir)+"'>"+value.sehir+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(value.brans)+"'>"+value.brans+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(teams[0])+"'>"+teams[0]+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(teams[1])+"'>"+teams[1]+"</a></td><td>"+value.sonuc+"</td></tr>")
                        });
                        return false;
                }
            });
    	}
    	else{
    		$('#kategori-div-sonuclar').show();
    		var catArr = [];
    		jQuery.ajax({
	        	type: 'POST',
	        	url: ajaxurl,
	        	data: {"action": "load-filter", cat: catID },
	        		success: function(response) {
	        			var json = JSON.parse(response);
	            		$.each(json,function(key,value){
	            			if($('#parent_'+value.parent).length > 0)
	            				$('#parent_'+value.parent).append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			else{
	            				if(value.description == "false")
	            					$('#ys-brans-kategori-sonuclar').append("<optgroup id='parent_"+value.term_id+"' label='"+value.name+"'></optgroup>");
	            				else
	            					$('#ys-brans-kategori-sonuclar').append('<option value="'+value.term_id+'">'+value.name+"</option>");
	            			}
	            		});
	            		$('#ys-brans-kategori-sonuclar').removeAttr('disabled');
	            		return false;
	        	}
    		});
    	}
    });
	$('#ys-brans-kategori-sonuclar').change(function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter2", cat: catID },
        		success: function(response) {
					var json = JSON.parse(response);
                        $('#sonuclar table tbody').html('');
                        $.each(json,function(key,value){
                            var teams = value.post_title.split("-");
                            $('#sonuclar table tbody').append("<tr><th scope='row'><a href='"+value.permalink+"'>"+value.tarih+"</a></th><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(value.sehir)+"'>"+value.sehir+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(value.brans)+"'>"+value.brans+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(teams[0])+"'>"+teams[0]+"</a></td><td><a href='/yarisma-icin-arama/?tag="+string_to_slug(teams[1])+"'>"+teams[1]+"</a></td><td>"+value.sonuc+"</td></tr>")
                        });
            		return false;
        	}
    		});
	});
	    $('#ks-brans').change(function(){
    	var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter", cat: catID },
        		success: function(response) {
        			$('#ks-lig').html('<option>Lig Seçiniz</option>');

        			var cats = JSON.parse(response);
        			$.each(cats,function(key,value){
        				if(value.parent == catID)
            				$('#ks-lig').append('<option value="'+value.term_id+'" data-son="'+value.description+'">'+value.name+"</option>");
        			});
        			$('#ks-lig').removeAttr('disabled');
            		return false;
        	}
    	});
    });
	$('#ks-lig').change(function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	var catID = $(this).val();
		jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filter", cat: catID },
        		success: function(response) {
        			
        			$('#liglerListe').html('');
        			var cats = JSON.parse(response);

        			$.each(cats,function(a,b){
        				if(b.parent == catID && b.description == "false"){
        				var string = "<div class='col-md-4' style='text-align:center;'>";
        				string += "<a href='javascript:void(0);' class='open-league' data-catid='"+b.term_id+"' data-catname='"+b.name+"'><img style='width:100%;height:auto;' src='/wp-content/themes/simplissimo-2/assets/images/kupaIcin.jpg'/></a><br/>";
        				string += "<span class='tags' style='clear:left;float:none;'><span>"+b.brans+"</span></span>";
        				string += "<p class='tittle' style='text-align:center!important;'><a href='javascript:void(0);' class='open-league' data-catid='"+b.term_id+"' data-catname='"+b.name+"'>"+b.name+"</a></p>";
        				string += "</div>";
        				$('#liglerListe').append(string);
        				}
        			});
            		return false;
        	}
    	});
	});
	var catID = 0;
	var cat_name = "";
	$(document).on('click','.open-league',function(){
		cat_name = $(this).attr('data-catname');
		var ajaxurl = '/wp-admin/admin-ajax.php';
		$('#catName').html($(this).attr('data-catname')).addClass('active').css('font-weight','700');
		$('.ys-content').removeClass('col-md-12').addClass('col-md-10');
		$('.ys-menu-outer').show();
		$('.startDiv').hide();
		$('.leagueDiv').show();
		if($('#ks-brans option:selected').html() == "Branş Seçiniz"){
				$('#catName').html($(this).parent().parent().find('.tags span').html()).addClass('active').css('font-weight','700');

		}
		catID = $(this).data('catid');
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filtercat", cat: catID },
        		success: function(response) {
        			var posts = JSON.parse(response);
        			$.each(posts,function(a,b){
        				$('.fikstur-div').append(b.post_content);
        			});
        			$('.fikstur-li').addClass('active');
            		return false;
        	}
    	});
	});
	$(document).on('click','.fikstur-li',function(){
		$('.lili').removeClass('active');
		$(this).addClass('active');

		var ajaxurl = '/wp-admin/admin-ajax.php';
		jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filtercat", cat: catID },
        		success: function(response) {
        			$('.contentDiv').hide(); 
        			$('.fikstur-div').html(''); 
        			var posts = JSON.parse(response);
        			$.each(posts,function(a,b){
        				$('.fikstur-div').append(b.post_content);
        			});
        			$('.fikstur-li').addClass('active');
        			$('.fikstur-div').show();
        			$('#pageTitle').html("Fikstür-<span>"+cat_name+"</span>");
            		return false;
        	}
    	});
	});
	$(document).on('click','.haber-li',function(){
		$('.lili').removeClass('active');
		$(this).addClass('active');
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filterhaber", cat: catID },
        	success: function(response) {        
        		$('.contentDiv').hide();    		
        		var json = JSON.parse(response);
        			$('.haber-div').html('');
        			$.each(json,function(key,value){
        				var tarih = new Date(value.post_date);
        				var string = "<div class='post-"+value.ID+" post'>";
        				string += "<ul><a href="+value.permalink+"><li> <img width='785' height='492' src="+value.thumbnail+"></a></li></ul>";
        				string += "<article><ul class='row'> <li class='col-sm-3'><div class='date'><span class='day'>"+weekday[tarih.getDay()-1]+"</span><span class='big'>"+tarih.getDate()+"</span><span>"+months[tarih.getMonth()]+" "+tarih.getFullYear()+"</span></div></li><li class='col-sm-9'><span class='tags'>";
        				$.each(value.tagss,function(a,b){
        					string += '<span><a href="/tag/'+b.slug+'/" rel="tag">'+b.name+'</a></span>';
        				});
        				string += '</span><h4><a href="'+value.permalink+'" class="tittle">'+value.post_title+'</a></h4>';
        				string += '<p class="excerpt">'+value.post_content.substr(0,250)+' [...]</p>';
        				string += '<a class="btn" href="'+value.permalink+'">DEVAMINI OKU</a></li>';
        				string += '</ul></article></div>';
        				$('.haber-div').append(string);
        			});
					$('#pageTitle').html("Haberler-<span>"+cat_name+"</span>");
            		$('.haber-div').show();
            		return false;
        	}
    	});
	});



	$(document).on('click','.sonuc-li',function(){
		$('.lili').removeClass('active');
		$(this).addClass('active');
		var ajaxurl = '/wp-admin/admin-ajax.php';
    	jQuery.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filtersonuc", cat: catID },
        	success: function(response) {        
        		$('.contentDiv').hide();    		
        		var json = JSON.parse(response);
        			$('.haber-div').html('');
        			$.each(json,function(key,value){
        				var teams = value.post_title.split('-');
        				var string = "<tr><th scope='row'><a href='"+value.permalink+"'"+value.tarih+">"+value.tarih+"</a></th>";
							string += "<td><a href='/kupa-icin-arama/?tag="+string_to_slug(value.sehir)+"'>"+value.sehir+"</a></td>";
							string += "<td><a href='/kupa-icin-arama/?tag="+string_to_slug(value.brans)+"'>"+value.brans+"</a></td>";
							string += "<td><a href='/kupa-icin-arama/?tag="+string_to_slug(teams[0])+"'>"+teams[0]+"</td>";
							string += "<td><a href='/kupa-icin-arama/?tag="+string_to_slug(teams[1])+"'>"+teams[1]+"</td>";
							string += "<td>"+value.sonuc+"</td></tr>";
        				$('.sonuc-div table tbody').append(string);
        			});
        			$('#pageTitle').html("Sonuçlar-<span>"+cat_name+"</span>");
        			$('.sonuc-div').show();
            		return false;
        	}
    	});
	});
	$(document).on('click','.puan-li',function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
		$('.lili').removeClass('active');
		$(this).addClass('active');
		$.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filterpuan", cat: catID },
        		success: function(response) {
        			$('.contentDiv').hide(); 
        			$('.puan-div').html('');
        			var posts = JSON.parse(response);
        			$.each(posts,function(a,b){
        				$('.puan-div').append(b.post_content);
        			});
        			$('.puan-li').addClass('active');
        			$('.puan-div').show();
        			$('#pageTitle').html("Puan Durumu-<span>"+cat_name+"</span>");
            		return false;
        	}
    	});
	});
	$(document).on('click','.galeri-li',function(){
		$('#pageTitle').html("Galeri-<span>"+cat_name+"</span>");
		$('.lili').removeClass('active');
		$('.contentDiv').hide(); 
        
        $('.galeri-li').addClass('active');
        $('.galeri-div').show();
	});

	$(document).on('click','.send-mail',function(){
		var ajaxurl = '/wp-admin/admin-ajax.php';
		var from= $('#cemail').val();
		var name = $('#cname').val();
		var phone = $('#cphone').val();
		var subject = $('#csubject').val(); 
		var message = $('#cmessage').val(); 
		$.ajax({
        	type: 'POST',
        	url: ajaxurl,
        	data: {"action": "load-filtermail",from:from,name:name,phone:phone,subject:subject,message:message},
        	success: function(response) {
        		alert('Mesajınız tarafımıza ulaşmıştır.');
        	}
    	});
	});


});
})(jQuery);

