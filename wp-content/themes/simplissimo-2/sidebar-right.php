<?php

/**
* Maybe show the right sidebar
*/
// $sidebar_class = ( layers_can_show_sidebar( 'left-sidebar' ) ? 'span-3' : 'span-4' );

//layers_maybe_get_sidebar( 'right-sidebar', 'column sidebar no-gutter ' );
//
?>

<?php $col_4 = is_home() ? $col_4 = ' column span-4':''; ?>

<div id="widget-area" class="widget-area right-sidebar<?php echo esc_attr($col_4);?>" role="complementary">
    <?php dynamic_sidebar( 'right-sidebar' ); ?>
</div><!-- .widget-area -->