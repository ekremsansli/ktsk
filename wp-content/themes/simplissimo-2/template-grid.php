<?php
/**
 * Template Name: Grid blog
 * The template for displaying grid blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
	<div class="container">
		<section class="post-sec full-posts">
			<div class="row">

				<!--======= POST SECTION =========-->
				<div class="col-md-12 grid-isotope">
						<?php
						if ( is_front_page() ) {
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						}
						$args = array(
							"post_type" => "post",
							"paged" => $paged
						);

						$wp_query = new WP_Query($args);

						if( $wp_query->have_posts() ) : ?>
							<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
								global $post; ?>
								<?php get_template_part( 'partials/content' , 'gridlist' ); ?>
							<?php endwhile; // while has_post(); ?>

						<?php endif; // if has_post() ?>
				</div>
				<?php the_posts_pagination(); ?>
			</div>
		</section>
	</div>
<?php get_footer();