<?php
/**
 * Template Name: Full width blog
 * The template for displaying full width blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
    <div class="container">
      <section class="post-sec full-posts">
        <div class="row">

          <!--======= POST SECTION =========-->
          <div class="col-md-12" style="background:white;">
          	<div class="col-md-12 col-sm-12 col-xs-12 contact-map" id="map-canvas-contact" style="border-top:1px solid white;" data-lat="41.027205" data-lng="29.042059" data-string="Koç Topluluğu Spor Kulübü" data-zoom="15">
          	</div>
            <div class="col-md-6 content-margined" >
              <form class="form-inline">
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" id="cname" class="form-control full-width cinput" placeholder="* Adınız ve Soyadınız">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" id="cemail" class="form-control full-width cinput" placeholder="* E-mail">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" id="cphone" class="form-control full-width cinput" placeholder="* Telefon Numarası">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" id="csubject" class="form-control full-width cinput" placeholder="* Mesaj Başlığı">
                  </div>
                  <div class="col-sm-12">
                    <textarea class="ctext" id="cmessage" placeholder="* Mesaj"></textarea>
                  </div>
                  <div class="col-sm-9"></div>
                  <div class="col-sm-3">
                    <div class=" hbtn send-mail" style="margin-left:42px!important;">
                      <a href="javascript:void(0);">
                        <p>GÖNDER</p>
                      </a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-6 content-margined">
              <div class="info-box col-md-12">
                <p class="title">Adres</p>
                <p>
                  Kuşbakışı Cad. 34398 Üsküdar / İstanbul , Türkiye<br/>
                  <span style="color:#e50b1c;">Tel: </span>+90 216 343 36 76<span style="color:#e50b1c;">&nbsp&nbsp&nbspFax: </span>+90 216 343 36 77
                </p>
              </div>
               <div class="info-box col-md-12">
                <p class="title">Web</p>
                <p>
                  <ul class="social_icons" style="text-align:left!important;">
                              <li class="facebook"><a href="https://www.facebook.com/koc.toplulugu.spor.kulubu/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                              <li class="youtube"><a href="https://www.youtube.com/user/ktsk2012" target="_blank"><i class="fa fa-youtube"></i></a></li>
                              <li class="instagram"><a href="https://www.instagram.com/koc_toplulugu_spor_kulubu/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                          </ul>
                  <br/>
                  <span style="color:#e50b1c;">E-mail: </span>info@ktsk.com.tr
                </p>
              </div>
              <div class="info-box col-md-12">
                <p class="title">Hesap Bilgileri</p>
                <p>
                  KOÇ TOPLULUĞU SPOR KULÜBÜ DERNEĞİ İKTİSADİ İŞLETMESİ<br/>
                  YKB NAKKAŞTEPE ŞUBESİ ( 930 )<br/>
                  HESAP NUMARASI 90 333593<br/>
                  İBAN: TR10 0006 7010 0000 0090 3335 93
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
<?php get_footer();
