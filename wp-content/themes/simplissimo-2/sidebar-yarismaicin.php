<?php $col_4 = is_home() ? $col_4 = ' column span-4':''; ?>
<div id="widget-area" class="widget-area right-sidebar<?php echo esc_attr($col_4);?>" role="complementary">
	<?php echo do_shortcode('[tribe_mini_calendar category="yetiskinler,cocuklar"]');?> 
	<div class="container">
		<div class="row">
			<div class="col-md-12 sidebar-tag">
				<div style="margin-bottom:25px;"><span class="title">TAGLER</span></div>

				<?php 
				
					$tags = wp_get_post_tags($post->ID);
					if(empty($tags)){
						$tags = wp_get_post_tags(526);
					}
					foreach($tags as $tag){
						echo '<a href="'.get_tag_link($tag->term_id).'" style="color:black;"><div class="tag col-md-3">'.$tag->name.'</div></a>';
					}
				?>
			</div>
		</div>
	</div>
</div>