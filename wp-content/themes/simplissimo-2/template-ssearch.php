<?php
/**
 * Template Name: Yarışma İçin Arama
 * The template for displaying imagewall blog
 *
 * @package Simplissimo
 * @since Simplissimo 1.0.0
 */

get_header(); ?>
<div class="container">
	<section class="post-sec">
		<div class="row">

			<!--======= POST SECTION =========-->
			
			<div class="col-md-8">
				<?php
				$args = array(
					"post_type" => "post",
					"paged" => $paged,
					'cat' => '132,140,145',
					'tag' => $_GET['tag']
				);
				print_r(wp_get_post_tags(642));
				$wp_query = new WP_Query($args);

				if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
						global $post; 
						?>
						<?php get_template_part( 'partials/content' , 'list' ); ?>
					<?php endwhile; // while has_post(); ?>

					<?php the_posts_pagination(); ?>
				<?php endif; // if has_post() ?>
			</div>

			<!--======= RIGHT SIDE BAR =========-->
			<div class="col-md-4">
				<div class="right-bar">
					<?php get_sidebar( 'cocuk' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();