<?php $col_4 = is_home() ? $col_4 = ' column span-4':''; ?>
<div id="widget-area" class="widget-area right-sidebar<?php echo esc_attr($col_4);?>" role="complementary">
<?php   //the_widget( 'Tribe__Events__Pro__Mini_Calendar_Widget' );
		echo do_shortcode('[tribe_mini_calendar category="yetiskinler,cocuklar"]');
		//tribe_get_template_part( 'pro/widgets/mini-calendar/list', null, array( 'venue' => true ) );

 ?> 
<?php 
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 1',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post1 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 2',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post2 = get_posts( $posts_args )[0];
	
	$posts_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category_name'         => 'Sidebar Güncel 3',
		'orderby'          => 'post_date',
		'order'            => 'ASC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$post3 = get_posts( $posts_args )[0];
?>
<div class="container">
	<div class="row">
		<div class="col-md-12 guncel-sidebar">
			<div class="header col-md-12">
				<span>GÜNCEL HABERLER</span>
			</div>
			<div class="inner col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a  href="<?=get_permalink($post1->ID)?>"><?=get_the_post_thumbnail($post1->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post1->ID)?>" title="<?=$post1->post_title?>"><?=$post1->post_title?></a></span>
						<p><?=get_excerpt(60,$post1->ID,$post1->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a  href="<?=get_permalink($post2->ID)?>"><?=get_the_post_thumbnail($post2->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post2->ID)?>" title="<?=$post2->post_title?>"><?=$post2->post_title?></a></span>
						<p><?=get_excerpt(60,$post2->ID,$post2->post_content);?></p>
					</div>
				</div>
				<div class="gpost col-md-12 col-sm-12 col-xs-12" style="padding: 0!important;">
					<div class="gimg col-sm-6"><a  href="<?=get_permalink($post3->ID)?>"><?=get_the_post_thumbnail($post3->ID,array(100,100));?></a></div>
					<div class="gcontent col-sm-6">
						<span class="gtitle"><a class="tittle" href="<?=get_permalink($post3->ID)?>" title="<?=$post3->post_title?>"><?=$post3->post_title?></a></span>
						<p><?=get_excerpt(60,$post3->ID,$post3->post_content);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
